(function() {
  'use strict';

  angular
    .module('app.core', [
      'ui.router',
      'base64',
      'gridshore.c3js.chart',
      'ngNotify',
      'ngLodash',
      'ui.mask',
      'ng-reactions',
      'angular-svg-round-progressbar',
      'pascalprecht.translate',
      'angular.filter',
      'dx'
    ]);

})();
