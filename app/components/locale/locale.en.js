(function () {
  'use strict';

  angular
    .module('app.locale')
    .config(config);


  config.$inject = ['$translateProvider'];

  /* @ngInject */
  function config($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.translations('en', {
      // Evaluator types
      Jefe: 'Leader',
      Par: 'Peer',
      Colaborador: 'Collaborator',
      Otro: 'Other',
      Auto: 'Autoevaluation',

      // Login page
      USERNAME: 'Username',
      USERNAME_ERROR: 'Username required',

      PASSWORD: 'Password',
      PASSWORD_ERROR: 'Password required',

      LOGIN_BUTTON: 'Log In',
      LOGIN_OFFICE365: 'Office365 Login',

      RECOVER_PASSWORD: 'Recover Password',
      FORGET_PASSWORD: 'Forget Password',
      RECOVER_CONFIRMATION: 'Are you sure you want to recover your password?',

      SESSION_REQUIRED: 'Session required',

      // Dashboard page
      DASHBOARD_HEADER: 'Dashboard',
      PERSONAL_EV_PROGRESS: 'Progress on personal evaluation',
      EV_STEPS: 'Evaluation steps',
      EV_STEP_1: 'Evaluation begins(add evaluators)',
      STARTED: 'Evaluation started at HR',
      EV_STEP_2: 'Invitations sent to evaluators',
      INVITATIONS_SENT: 'Invitations sent to evaluators',
      EV_STEP_3: 'Evaluation in process',
      IN_PROGRESS: 'Evaluation started',
      EV_STEP_4: 'Evaluation completed (answers from evaluators and auto-evaluation)',
      COMPLETED: 'Evaluation completed',
      PENDING_EVS: 'Pending evaluations',
      PENDING_EVS_EXPLANATION: 'These surveys generate feedback for someone else.',
      USER_EVALUATIONS: 'My evaluations',
      USER_EVS_EXPLANATION: 'These evaluations give feedback about yourself.',
      EVALUATION_BUTTON: 'Evaluate',
      INFO_VIDEO_1: '360 Feedback',
      INFO_VIDEO_2: 'How to interpret results',
      VIEW_EV_DETAILS: 'View details',

      // Evaluator status
      STARTED: 'Pending',
      COMPLETED: 'Completed survey',

      WELCOME: '360 Feedback System',
      WELCOME_MSG: 'Welcome to the 360 Feedback System, designed to meassure the perception that your leaders, peers, collaborators, internal clients and other have about the behaviour you have in the work environment regarding FIFCO`s Leadership Model.',
      WELCOME_MSG_2: 'We cordially invite you to watch the following videos in order to learn more about how to apply this process.',
      ADD_EVALUATORS_ERROR: 'You must add at least one leader, two peers or clients and 2 collaborators',

      // Admin
      CLOSE_BUTTON: 'Close',

      // Survey pages
      SURVEY_HEADER: 'Surveys',
      ADD_SURVEY: 'Add survey',
      EDIT_SURVEY: 'Edit survey',
      ERROR_LOADING_SURVEYS: 'Error loading surveys',
      ERROR_LOADING_SURVEY: 'Error loading survey',
      CREATE_SURVEY: 'Survey has been created successfully.',
      SURVEY_UPDATED: 'Survey has been updated successfully.',
      SURVEY_DELETED: 'Survey has been deleted successfully.',
      SURVEY_NAME: 'Name',

      // Topic pages
      TOPIC_HEADER: 'Topics',
      ADD_TOPIC: 'Add topic',
      EDIT_TOPIC: 'Edit topic',
      ERROR_LOADING_TOPICS: 'Error loading topics',
      TOPIC_NAME: 'Name',
      TOPIC_CREATED: 'Topic has been created successfully. Click refresh to see the changes.',
      TOPIC_UPDATED: 'Topic has been updated successfully',
      TOPIC_DELETED: 'Topic has been deleted successfully',

      // Subtopic pages
      SUBTOPIC_HEADER: 'Subtopics',
      ADD_SUBTOPIC: 'Add subtopic',
      EDIT_SUBTOPIC: 'Edit subtopic',
      ERROR_LOADING_SUBTOPICS: 'Error loading subtopics',
      CREATED_SUBTOPIC: 'Subtopic has been created successfully. Click refresh to see the changes.',
      UPDATED_SUBTOPIC: 'Subtopic has been updated successfully',
      DELETED_SUBTOPIC: 'Subtopic has been deleted successfully',

      SUBTOPIC_NAME: 'Name',

      // Evaluation pages
      EVALUATION_HEADER: 'Evaluations',
      ADD_EVALUATION: 'Add evaluation',
      EDIT_EVALUATION: 'Edit evaluation',
      ERROR_LOADING_EVALUATIONS: 'Error loading evaluations',
      ERROR_LOADING_EVALUATION: 'Error loading evaluations',
      EVALUATION_ADDED: 'Evaluation has been created successfully.',
      EVALUATION_UPDATED: 'Evaluation has been updated successfully',
      EVALUATION_DELETED: 'Evaluation has been deleted successfully',
      EVALUATION_RESULT: 'View results',
      EVALUATION_RESULT_BY_DATE: 'View results by date',
      EVALUATION_NAME: 'Evaluation name',
      EVALUATION_SURVEY: 'Select survey',
      EVALUATION_START_DATE: 'Start date',
      EVALUATION_END_DATE: 'End date',
      EVALUATION_EMAILS: 'Employee emails (only emails with @fifco.com, @nabreweries.com or @reservaconchal.com domains).Separate emails using comma',
      EVALUATION_CALENDAR_TODAY: 'Today',
      EVALUATION_CALENDAR_CLEAR: 'Clear',
      EVALUATION_INSTRUCTIONS: 'Welcome to the process of creating evaluations. Given the systems support of both English and Spanish languages, it is necessary that you type in an evaluation name in spanish (ES) and in english (EN), along with the selection of a survey from the dropdown list, a start date and an end date for the evaluation and a list of employee emails to be evaluated separated by comas.',
      EVALUATION_INFO: 'Evaluation details',
      EVALUATION_DOWNLOAD_REPORT: 'Download report as PDF',
      EVALUATION_COMPLETED: 'Completed evaluation.',
      EVALUATION_IN_PROGRESS: 'This evaluation is already completed',
      EVALUATION_OUT_OF_DATE: 'This evaluation is already expired',
      EVALUATION_INVALID_KEY: 'Invalid evaluation key',
      REPORT_GENERATED: 'Report generated.',
      FINISH_EVALUATION: 'Finish evaluation',
      FINISH_EVALUATION_INFO: 'This button will allow you finish feedback process for emitting the final report',
      CANCEL: 'Cancel',
      WARNING: 'Warning!',
      WARNING_TEXT: 'Are you sure you want to terminate the evaluation? Some evaluators have not submitted their answers, which could affect the results.',
      USER_RESULTS: 'Date results',
      ERROR_LOADING_MANAGERS: 'Error loading managers',
      MANAGER_ADDED: 'Manager has been created successfully',
      MANAGER_DELETED: 'Manager has been deleted successfully',
      MANAGER_UPDATED: 'Manager has been updated successfully',
      ERROR_LOADING_COMPANIES: 'Error loading companies',
      ERROR_LOADING_USERS: 'Error loading users',
      ERROR_LOADING_USERS_TYPES: 'Error loading user types',
      USER_ADDED: 'User has been created successfully',
      USER_UPDATED: 'User has been updated successfully',
      USER_DELETED: 'User has been deleted successfully',
      ERROR_LOADING_INDICATORS: 'Error loading indicators',
      UPDATE_EVALUATION: 'Update',

      ADD_EVALUATOR: 'Add evaluators',
      NEW_EVALUATOR: 'New evaluator',
      EVALUATORS: 'Evaluators',
      EVALUATOR_NAME: 'Name',
      EVALUATOR_EMAIL: 'Email',
      EVALUATOR_LIMITS: 'Invite at least 1 leader, 3 collaborators and 3 peers or 3 internal clients.',
      EVALUATORS_INVITED: 'Evaluators invited successfully.',
      EVALUATOR_REPEATED: 'Evaluators only must have one role',

      // Question pages
      QUESTION_HEADER: '',
      ADD_QUESTION: 'Add question',
      EDIT_QUESTION: 'Edit',

      QUESTION_NAME: 'Question statement',
      QUESTION_TYPE_CLOSED: 'Closed question (ranked)',
      QUESTION_TYPE_OPEN: 'Open question',
      ERROR_LOADING_QUESTIONS: 'Error loading questions',
      ERROR_LOADING_QUESTION_TYPES: 'Error loading question types',
      QUESTION_ADDED: 'Question has been created successfully. Click refresh to see the changes.',
      QUESTION_UPDATED: 'Question has been updated successfully',
      QUESTION_DELETED: 'Question has been deleted successfully',

      // Form
      SEND_ANSWERS: 'Submit answers',
      SUCESS_SENDING_ANSWERS: 'Answers send correctly',
      ERROR_SENDING_ANSWERS: 'Error submitting your answers',
      INSTRUCTIONS_LABEL: 'Instructions',
      INSTRUCTIONS: 'In FIFCO we want to support our leaders to grow according to our leadership model. Please complete this survey regarding the person in the email. Several people, like yourself, will also provide feedback and the system will capture and analyse the data. We thank you for taking a few minutes to share your perception of him/her. Your answers will be treated with absolute confidentiality. Select the answer that reflects the most your experience with him/her in the corresponding box.',
      SCALE: 'The rating scale used for the answers goes from 1 (never) to 5 (always).',
      THANKS: 'Thanks for your participation.',
      THANKS_2: 'Your results will be processed and integrated along with the other participants to generate a report with the evaluation results.',
      BLANK_FORM_ERROR: 'You need to answer all the questions',
      EXPIRED_TEXT: "Expired evaluation",
      SAVE_AS_DRAFT: "Save As Draft",

      //Top nav bar
      NAV_SURVEY: 'Surveys',
      NAV_EVALUATION: 'Evaluations',
      NAV_REFRESH: 'Refresh',

      // Nav left panel
      DASHBOARD: 'Dashboard',
      ADMIN: 'Administrator',
      LOGOUT_BUTTON: 'Log out',
      CHANGE_PASSWORD: 'Change password',

      // Modal reset password
      CURRENT_PASSWORD: 'Current password',
      NEW_PASSWORD: 'New password',
      CONFIRM_PASSWORD: 'Confirm new password',
      ERROR_NEW_PASSWORD: 'The password must have at least 6 characters',
      ERROR_CURRENT_PASSWORD: 'The current password is required',
      PASSWORD_CHANGED: 'The password was successfully changed',
      WRONG_PASSWORD: 'Wrong current password',
      DIFERENT_PASSWORDS: 'The new passwords do not match',
      SAME_PASSWORDS: 'The current and the new password can not be the same',
      PASSWORD_RESET_EMAIL: 'Recover email sent to: ',
      USER: 'User ',
      NOT_EXIST: 'not found ',
      ERROR_RESTORING_PASSWORD: 'Error restoring the password',
      LOGIN_ERROR: 'Wrong username or password.',

      ERROR: 'Something went wrong. Please try again.',
      PERMISSION_ERROR: 'Insufficient permissions',
    });
  }
})();
