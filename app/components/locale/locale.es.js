(function () {
  'use strict';

  angular
    .module('app.locale')
    .config(config);


  config.$inject = ['$translateProvider'];

  /* @ngInject */
  function config($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.translations('es', {
      // Evaluator types
      Jefe: 'Jefe',
      Par: 'Par',
      Colaborador: 'Colaborador',
      Otro: 'Otro',
      Auto: 'Autoevaluación',

      // Login page
      USERNAME: 'Nombre de usuario',
      USERNAME_ERROR: 'Nombre de usuario requerido',

      PASSWORD: 'Contraseña',
      PASSWORD_ERROR: 'Contraseña requerida',

      LOGIN_BUTTON: 'Ingresar',
      LOGIN_OFFICE365: 'Ingresar con Office365',

      RECOVER_PASSWORD: 'Restaurar contraseña',
      FORGET_PASSWORD: 'Olvidé la contraseña',
      RECOVER_CONFIRMATION: 'Está seguro que desea restaurar su contraseña?',

      SESSION_REQUIRED: 'Sesion requerida',

      WELCOME: 'Sistema de Retroalimentación 360',
      WELCOME_MSG: 'Reciba la más cordial bienvenida al Sistema de Retroalimentación 360, diseñado para medir las percepciones de su líder, pares, colaboradores, clientes internos y otros, acerca de los comportamientos que usted demuestra en su contexto laboral en relación con el Modelo de Liderazgo de FIFCO.',
      WELCOME_MSG_2: 'Le invitamos a observar los siguientes videos para aprender más sobre cómo aplicar este proceso.',
      ADD_EVALUATORS_ERROR: 'Debe agregar al menos un jefe, 2 colaboradores y 2 pares o clientes',

      // Dashboard page
      DASHBOARD_HEADER: 'Dashboard',
      PERSONAL_EV_PROGRESS: 'Progreso de evaluación personal',
      EV_STEPS: 'Pasos del proceso de evaluación',
      EV_STEP_1: 'Inicio de Evaluación (ingreso de evaluadores)',
      STARTED: 'Evaluación iniciada en RH',
      EV_STEP_2: 'Invitaciones enviadas a los evaluadores',
      INVITATIONS_SENT: 'Invitaciones enviadas a evaluadores',
      EV_STEP_3: 'Evaluación en proceso',
      IN_PROGRESS: 'Evaluación iniciada',
      EV_STEP_4: 'Evaluación completada (respuestas de evaluadores y autoevaluación)',
      COMPLETED: 'Evaluación completada',
      PENDING_EVS: 'Evaluaciones por realizar',
      PENDING_EVS_EXPLANATION: 'Estas evaluaciones proporcionan retroalimentación para alguien más.',
      USER_EVALUATIONS: 'Mis evaluaciones',
      USER_EVS_EXPLANATION: 'Evaluaciones que generan retroalimentación propia.',
      EVALUATION_BUTTON: 'Evaluar',
      INFO_VIDEO_1: 'Retroalimentacion 360',
      INFO_VIDEO_2: 'Como interpretar resultados',
      VIEW_EV_DETAILS: 'Ver detalles',

      // Evaluator status
      STARTED: 'Pendiente',
      COMPLETED: 'Formulario completado',

      // Admin
      CLOSE_BUTTON: 'Salir',

      // Survey pages
      SURVEY_HEADER: 'Cuestionarios',
      ADD_SURVEY: 'Agregar cuestionario',
      EDIT_SURVEY: 'Editar cuestionario',
      DELETE_SURVEY: 'Eliminar cuestionario',
      ERROR_LOADING_SURVEYS: 'Error cargando cuestionarios',
      CREATE_SURVEY: 'Cuestionario creado satisfactoriamente.',
      SURVEY_UPDATED: 'Cuestionario actualizado satisfactoriamente.',
      SURVEY_DELETED: 'Cuestionario eliminado satisfactoriamente',
      ERROR_LOADING_SURVEY: 'Error al cargar el cuestionario',

      SURVEY_NAME: 'Nombre',

      // Topic pages
      TOPIC_HEADER: 'Temas',
      ADD_TOPIC: 'Agregar tema',
      EDIT_TOPIC: 'Editar tema',
      ERROR_LOADING_TOPICS: 'Error cargando temas',
      TOPIC_NAME: 'Nombre',
      TOPIC_CREATED: 'El tema ha sido creado satisfactoriamente. Refresque la página para ver los cambios.',
      TOPIC_UPDATED: 'Tema actualizado satisfactoriamente',
      TOPIC_DELETED: 'Tema eliminado satisfactoriamente',

      // Subtopic pages
      SUBTOPIC_HEADER: 'Subtemas',
      ADD_SUBTOPIC: 'Agregar subtema',
      EDIT_SUBTOPIC: 'Editar subtema',
      ERROR_LOADING_SUBTOPICS: 'Error al cargar los subtemas',
      CREATED_SUBTOPIC: 'Subtema creado satisfactoriamente. Click en refrescar para ver los cambios.',
      UPDATED_SUBTOPIC: 'Subtema actualizado satisfactoriamente',
      DELETED_SUBTOPIC: 'Subtema eliminado satisfactoriamente',

      SUBTOPIC_NAME: 'Nombre',

      // Evaluation pages
      EVALUATION_HEADER: 'Evaluaciones',
      ADD_EVALUATION: 'Agregar evaluación',
      EDIT_EVALUATION: 'Editar evaluación',
      ERROR_LOADING_EVALUATIONS: 'Error cargando evaluaciones',
      ERROR_LOADING_EVALUATION: 'Error cargando evaluación',
      EVALUATION_ADDED: 'Evaluación creada satisfactoriamente.',
      EVALUATION_UPDATED: 'Evaluación actualizada satisfactoriamente.',
      EVALUATION_DELETED: 'Evaluación eliminada satisfactoriamente.',
      EVALUATION_RESULT: 'Ver resultados',
      EVALUATION_RESULT_BY_DATE: 'Ver resultados por fecha',
      EVALUATION_NAME: 'Nombre de la evaluación',
      EVALUATION_SURVEY: 'Seleccionar cuestionario',
      EVALUATION_START_DATE: 'Fecha de inicio',
      EVALUATION_END_DATE: 'Fecha de finalización',
      EVALUATION_EMAILS: 'Correos de los empleados a evaluar (solo correos con dominio @fifco.com, @nabreweries.com ó @reservaconchal.com).Debe de separar los correos con coma',
      EVALUATION_CALENDAR_TODAY: 'Hoy',
      EVALUATION_CALENDAR_CLEAR: 'Limpiar',
      EVALUATION_INSTRUCTIONS: 'Bienvenido al proceso de creación de evaluaciones. Dada la capacidad del sistema de manejar los idiomas inglés y español, es necesario que se ingrese un nombre para la evaluación en inglés (EN) y uno en español (ES), además de seleccionar un cuestionario de la lista de cuestionarios disponibles, una fecha de inicio y otra de finalización y la lista de correos electrónicos de los empleados a evaluar separados por comas.',
      EVALUATION_INFO: 'Detalles de la evaluación',
      EVALUATION_DOWNLOAD_REPORT: 'Guardar reporte como PDF',
      REPORT_GENERATED: 'Reporte generado.',
      EVALUATION_COMPLETED: 'Evaluación completada.',
      EVALUATION_IN_PROGRESS: 'Esta evaluación ya ha sido completada',
      EVALUATION_OUT_OF_DATE: 'La evaluación ha expirado',
      EVALUATION_INVALID_KEY: 'Clave inválida de evaluación',
      FINISH_EVALUATION: 'Terminar evaluación',
      FINISH_EVALUATION_INFO: 'Con este botón, usted finaliza el proceso de recolección de feedback para emitir el reporte final',
      CANCEL: 'Cancelar',
      WARNING: 'Advertencia!',
      WARNING_TEXT: 'Seguro quieres dar por finalizada la evaluación? Algunos evaluadores no han completado el cuestionario y esto podria afectar los resultados obtenidos.',
      USER_RESULTS: 'Resultados por Fecha',
      ERROR_LOADING_MANAGERS: 'Error cargando managers',
      MANAGER_ADDED: 'Manager ha sido creado satisfactoriamente',
      MANAGER_DELETED: 'El manager ha sido eliminado satisfactoriamente',
      MANAGER_UPDATED: 'El manager ha sido modificado satisfactoriamente',
      ERROR_LOADING_COMPANIES: 'Error cargando compañias',
      ERROR_LOADING_USERS: 'Error cargando usuarios',
      ERROR_LOADING_USERS_TYPES: 'Error cargando tipos de usuarios',
      USER_ADDED: 'Usuario creado satisfactoriamente',
      USER_UPDATED: 'Usuario actualizado satisfactoriamente',
      USER_DELETED: 'Usuario eliminado satisfactoriamente',
      ERROR_LOADING_INDICATORS: 'Error cargando los indicadores',
      UPDATE_EVALUATION: 'Editar',

      ADD_EVALUATOR: 'Agregar evaluadores',
      NEW_EVALUATOR: 'Nuevo evaluador',
      EVALUATORS: 'Evaluadores',
      EVALUATOR_NAME: 'Nombre',
      EVALUATOR_EMAIL: 'Correo',
      EVALUATOR_LIMITS: 'Invite al menos como mínimo 1 jefe, 3 colaboradores y 3 pares o clientes internos.',
      EVALUATORS_INVITED: 'Evaluadores invitados exitosamente.',
      EVALUATOR_REPEATED: 'Un evaluador solamente puede tener un rol asociado',

      // Question pages
      QUESTION_HEADER: '',
      ADD_QUESTION: 'Agregar pregunta',
      EDIT_QUESTION: 'Editar',

      QUESTION_NAME: 'Enunciado',
      QUESTION_TYPE_CLOSED: 'Pregunta cerrada (rango)',
      QUESTION_TYPE_OPEN: 'Pregunta abierta',
      ERROR_LOADING_QUESTIONS: 'Error cargando las preguntas',
      ERROR_LOADING_QUESTION_TYPES: 'Error cargando los tipos de preguntas',
      QUESTION_ADDED: 'La pregunta ha sido agregada satisfactoriamente. Refresque la pagina para ver los cambios.',
      QUESTION_UPDATED: 'La pregunta ha sido modificada satisfactoriamente',
      QUESTION_DELETED: 'La pregunta ha sido eliminada satisfactoriamente',

      // Form
      SEND_ANSWERS: 'Enviar respuestas',
      SUCESS_SENDING_ANSWERS: 'Respuestas enviando correctamente',
      ERROR_SENDING_ANSWERS: 'Error enviando sus respuestas',
      INSTRUCTIONS_LABEL: 'Instrucciones',
      INSTRUCTIONS: 'En FIFCO queremos apoyar a nuestros líderes para que puedan desarrollarse de acuerdo con nuestro modelo de liderazgo. Por favor llene esta encuesta pensando en la persona que se señala en el correo. Varias personas, al igual que usted, darán retroalimentación al mismo tiempo y un sistema consolidara los datos. Le agradecemos que tome unos minutos para que nos comparta su percepción sobre él o ella. Sus respuestas serán tratadas con absoluta confidencialidad. Marque la respuesta que más se ajuste a su experiencia con esta persona en la casilla correspondiente.',
      SCALE: 'La escala de medición  utilizada para la valoración de las las respuestas va desde 1 (nunca) hasta 5 (siempre).',
      THANKS: 'Gracias por su participación.',
      THANKS_2: 'Sus resultados serán procesados e integrados junto con el resto de participantes para la generación de un reporte con los resultados de la evaluación.',
      BLANK_FORM_ERROR: 'Debe de responder todas las preguntas',
      EXPIRED_TEXT: "Evaluación expirada",
      SAVE_AS_DRAFT: "Guardar Borrador",

      //Top nav bar
      NAV_SURVEY: 'Cuestionarios',
      NAV_EVALUATION: 'Evaluaciones',
      NAV_REFRESH: 'Refrescar',

      // Nav panel
      DASHBOARD: 'Dashboard',
      ADMIN: 'Administrador',
      LOGOUT_BUTTON: 'Salir',
      CHANGE_PASSWORD: 'Cambiar contraseña',

      // Modal reset password
      CURRENT_PASSWORD: 'Contraseña actual',
      NEW_PASSWORD: 'Nueva contraseña',
      CONFIRM_PASSWORD: 'Confirmar contraseña',
      ERROR_NEW_PASSWORD: 'La contraseña debe  al menos tener 6 caracteres',
      ERROR_CURRENT_PASSWORD: 'La contraseña actual es requerida',
      PASSWORD_CHANGED: 'La contraseña ha sido modificada exitosamente',
      WRONG_PASSWORD: 'La Contraseña actual es incorrecta',
      DIFERENT_PASSWORDS: 'Las nuevas contraseñas no coinciden',
      SAME_PASSWORDS: 'La nueva contraseña no puede ser igual a la actual',
      PASSWORD_RESET_EMAIL: 'Se ha enviado el correo a: ',
      USER: 'El usuario ',
      NOT_EXIST: 'no existe ',
      ERROR_RESTORING_PASSWORD: 'Error restaurando la contraseña',
      LOGIN_ERROR: 'El nombre de usuario o contraseña son incorrectos.',

      ERROR: 'Algo salio mal. Por favor intente de nuevo.',
      PERMISSION_ERROR: 'Permisos insuficientes',
    });
  }
})();
