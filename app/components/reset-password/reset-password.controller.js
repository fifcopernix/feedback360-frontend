(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller('ResetPasswordController', ResetPasswordController);

  /* @ngInject */
  function ResetPasswordController($uibModalInstance,
                                   ngNotify,
                                   UserService,
                                   $translate) {
    var vm = this;
    vm.closeModal = closeModal;
    vm.resetPassword = resetPassword;
    vm.currentLanguage = currentLanguage;
    vm.formNewPassword = {};
    vm.uibModalInstance = $uibModalInstance;
    vm.translate = $translate;

    function closeModal() {
      vm.uibModalInstance.dismiss('cancel');
    }

    function resetPassword() {
      var language = vm.currentLanguage();
      if (vm.formNewPassword.newPassword !== vm.formNewPassword.oldPassword) {
        if(vm.formNewPassword.newPassword === vm.confirmNewPassword) {
          vm.formNewPassword.userId = UserService.getCurrentUser().id;
          UserService.resetPassword(vm.formNewPassword)
            .then(response => {
              vm.translate(['PASSWORD_CHANGED']).then(translations => {
                ngNotify.set(translations.PASSWORD_CHANGED, 'success');
                closeModal();
              });
            })
            .catch(error => {
              if (error.status === 403) {
                vm.translate(['WRONG_PASSWORD']).then(translations => {
                  ngNotify.set(translations.WRONG_PASSWORD, 'error');
                });
              } else {
                ngNotify.set(error, 'error')
              }
            });
        } else {
          vm.translate(['DIFERENT_PASSWORDS']).then(translations => {
            ngNotify.set(translations.DIFERENT_PASSWORDS, 'error');
          });
        }
      } else {
        vm.translate(['SAME_PASSWORDS']).then(translations => {
          ngNotify.set(translations.SAME_PASSWORDS, 'error');
        });
       }

    }

    function currentLanguage() {
      return UserService.getLanguage();
    }
  }

})();
