(function () {
  'use strict';

  angular
    .module('app.resource')
    .constant('RESOURCE', {
      'API_URL': 'http://localhost:8080/',
      'API_CLIENT_AUTH': 'Basic ZmVlZGJhY2szNjB3ZWJjbGllbnQ6ZmVlRGI0Y2szNjB3ZWJjN2llbnQ=',
      'SCALE1': [1, 1.5, 2, 2.5],
      'SCALE2': [3, 3.5, 4, 4.5, 5],
      'filterName': 'date',
      'formatDate': 'yyyy/MM/dd',
      'ICONSET': [
        { icon: 'ra-medium reaction-happy', rating: 5, name: 'Happy' },
        { icon: 'ra-medium reaction-smile', rating: 4, name: 'Good' },
        { icon: 'ra-medium reaction-neutral', rating: 3, name: 'OK' },
        { icon: 'ra-medium reaction-sad', rating: 2, name: 'Bad' },
        { icon: 'ra-medium reaction-frustrated', rating: 1, name: 'Frustrated' }
      ]
    });

})();
