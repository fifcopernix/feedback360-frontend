"use strict";

(function () {
  angular
    .module('app')
    .service('GraphHelper', ['$http', function ($http, APPLICATION_CONFIG) {

      this.login = login;
      this.getPersonalInformation = getPersonalInformation;

      var applicationConfig = {
        clientID: 'ac842b7a-b6f0-4c13-a57e-66c1f32605ac',
        graphScopes: ["user.read"],
        redirectUri: "http://localhost:3000/login",
        interactionMode: "popUp",
        graphEndpoint: "https://graph.microsoft.com/v1.0/me",
      };

      var logger = new Msal.Logger(loggerCallback, { level: Msal.LogLevel.Verbose, correlationId:'12345', piiLoggingEnabled: true });
      
      function loggerCallback(logLevel, message, piiLoggingEnabled) {}

      var userAgentApplication = new Msal.UserAgentApplication(applicationConfig.clientID, null, authCallback, { logger: logger, cacheLocation: 'localStorage', loadFrameTimeout: 10000 });

      function authCallback(errorDesc, token, error, tokenType) {
        (token) ? console.log(error + ":" + errorDesc) : console.log(error + ":" + errorDesc);
      }
        
      function login() {
        sessionStorage.removeItem('user');
        localStorage.clear();
        return new Promise((resolve, reject) => {
          userAgentApplication.loginPopup(applicationConfig.graphScopes)
            .then(idToken => {
                userAgentApplication.acquireTokenSilent(applicationConfig.graphScopes)
                  .then(accessToken => {
                    sessionStorage.setItem('token', accessToken);
                    resolve();
                  }, 
                  error => {
                    userAgentApplication.acquireTokenPopup(applicationConfig.graphScopes)
                      .then(accessToken => {
                        sessionStorage.setItem('token', accessToken);
                        resolve();
                      },
                      error => {
                        reject();
                      });
                  });
            },
            error => {
              reject();
            }
          );
        })
      }

    function getPersonalInformation() {
      return $http.get('https://graph.microsoft.com/v1.0/me');
    }
  }]);
})();
