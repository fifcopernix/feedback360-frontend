(function() {
  'use strict';

  angular
    .module('app.service')
    .service('UserService', UserService);

  /* @ngInject */
  function UserService($http,
                       RESOURCE,
                       $window,
                       $state,
                       ngNotify,
                       $base64,
                       $translate) {

    this.getUserTypes = getUserTypes;
    this.getUserTypeByLogin = getUserTypeByLogin;
    this.getUsers = getUsers;
    this.getUser = getUser;
    this.createUser = createUser;
    this.updateUser = updateUser;
    this.setCurrentUser = setCurrentUser;
    this.getCurrentUser = getCurrentUser;
    this.clearCurrentUser = clearCurrentUser;
    this.changeStateUser = changeStateUser;
    this.isAdmin = isAdmin;
    this.verifyCredentials = verifyCredentials;
    this.getAuthorization = getAuthorization;
    this.setLanguage = setLanguage;
    this.getLanguage = getLanguage;
    this.recoverPassword = recoverPassword;
    this.resetPassword = resetPassword;
    this.setOffice365Credentials = setOffice365Credentials;
    this.retrieveOffice365Credentials = retrieveOffice365Credentials;
    this.translate = $translate;

    function setLanguage(lang) {
      sessionStorage.setItem('language', lang);
    }

    function getLanguage() {
      return sessionStorage.getItem('language');
    }

    function getUserTypes() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'userType',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        }
      };

      return $http(request);
    }
    function office365Login(){
      var request = {
        url: RESOURCE.API_URL + 'Office365Login',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        }
      };
    }
    function getUserTypeByLogin(username) {
      var usernameEncode = encodeURIComponent(username); // "%2B"
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'user-by-login?login=' + username,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        }
      };
      return $http(request);
    }

    function getUsers() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'users',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        }
      };

      return $http(request);
    }

    function getUser(user) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'user/' + user.id,
        headers: {
          'Content-Type': 'application/json'
        }
      };

      return $http(request);
    }

    function createUser(user) {
      var postRequest = {
        method: 'POST',
        url:  RESOURCE.API_URL + 'user',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        },
        data: user
      };

      return $http(postRequest);
    }

    function updateUser(user) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'user',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        },
        data: user
      };

      return $http(postRequest);
    }

    function setOffice365Credentials(user){
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'set-office365-credentials',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        },
        data: user
      };

      return $http(postRequest);
    }

    function retrieveOffice365Credentials(user) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'office365Login',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        },
        data: user
      };

      return $http(postRequest);
    }

    function changeStateUser(user) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'user/changeState',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        },
        data: user
      };

      return $http(postRequest);
    }

    function setCurrentUser(user) {
      sessionStorage.setItem('CurrentUser', JSON.stringify(user));
    }

    function getCurrentUser() {
      return JSON.parse(sessionStorage.getItem('CurrentUser'));
    }

    function clearCurrentUser() {
      setCurrentUser(null);
    }

    function isAdmin() {
      return getCurrentUser().role.name == 'ADMIN';
    }

    function verifyCredentials() {
      if (getCurrentUser() == null) {
        this.translate(['SESSION_REQUIRED']).then(translations => {
          ngNotify.set(translations.SESSION_REQUIRED, 'error');
        });
        $state.go('login');
      }
    }

    function getAuthorization() {
      var user = getCurrentUser();
      return 'Basic ' + $base64.encode(user.username + ':' + user.password);
    }

    function recoverPassword(userLogin, language) {
      var postRequest = {
        method: 'GET',
        url:  RESOURCE.API_URL + `password/recover/${userLogin}/${language}`,
        headers: {
          'Content-Type': 'application/json'
        }
      }
      return $http(postRequest);
    }

    function resetPassword(password) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'password',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': getAuthorization()
        },
        data: password
      };
      return $http(postRequest);
    }

  }

})();
