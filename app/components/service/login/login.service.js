(function() {
  'use strict';

  angular
    .module('app.service')
    .service('LoginService', LoginService);

  /* @ngInject */
  function LoginService($http, $httpParamSerializer, RESOURCE) {
    this.login = login;
    this.$http = $http;
    this.$httpParamSerializer = $httpParamSerializer;
    this.RESOURCE = RESOURCE;

    function login(username, password) {
      const request = {
        grant_type: 'password', // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        username, password
      };

      return this.$http({
        url: `${this.RESOURCE.API_URL}oauth/token`,
        method: 'POST',
        data: this.$httpParamSerializer(request),
        headers: {
          'Authorization': this.RESOURCE.API_CLIENT_AUTH,
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
        }
      })
    }
  }

})();
