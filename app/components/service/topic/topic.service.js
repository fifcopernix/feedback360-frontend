(function() {
  'use strict';

  angular
    .module('app.service')
    .service('TopicService', TopicService);

  /* @ngInject */
  function TopicService(UserService, $http, RESOURCE, $base64) {

    this.getTopics = getTopics;
    this.getTopic = getTopic;
    this.createTopic = createTopic;
    this.updateTopic = updateTopic;
    this.changeStateTopic = changeStateTopic;

    function getTopics() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'topic',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function getTopic(topic) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'topic/' + topic.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function createTopic(topic) {
      var postRequest = {
        method: 'POST',
        url:  RESOURCE.API_URL + 'topics',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: topic
      };

      return $http(postRequest);
    }

    function updateTopic(topic) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'topics',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: topic
      };

      return $http(postRequest);
    }

    function changeStateTopic(topic) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'topic/changeState',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: topic
      };

      return $http(postRequest);
    }

  }

})();
