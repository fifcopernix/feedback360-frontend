(function () {
  'use strict';

  angular
    .module('app.service')
    .service('EvaluationService', EvaluationService);

  /* @ngInject */
  function EvaluationService(UserService, $http, RESOURCE, $base64) {
    this.getEvaluations = getEvaluations;
    this.getEvaluationsByEvaluator = getEvaluationsByEvaluator;
    this.getEvaluationsByUser = getEvaluationsByUser;
    this.getEvaluationsByStage = getEvaluationsByStage;
    this.getEvaluation = getEvaluation;
    this.createEvaluation = createEvaluation;
    this.updateEvaluation = updateEvaluation;
    this.updateEvaluationStage = updateEvaluationStage;
    this.changeStateEvaluation = changeStateEvaluation;
    this.validateKey = validateKey;
    this.inviteEvaluators = inviteEvaluators;
    this.saveEvaluationToSession = saveEvaluationToSession;
    this.getSavedEvaluation = getSavedEvaluation;
    this.clearSessionEvaluation = clearSessionEvaluation;
    this.getEvaluationResults = getEvaluationResults;
    this.getEvaluationResultsCSV = getEvaluationResultsCSV;
    this.getCSVReportByUserDateRange = getCSVReportByUserDateRange;
    this.checkEvaluationCompleted = checkEvaluationCompleted;

    function inviteEvaluators(payload) {
      var request = {
        method: 'POST',
        url: RESOURCE.API_URL + 'evaluations-invite',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: payload
      };
      return $http(request);
    }

    function getEvaluations() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluations',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function getEvaluationsByStage() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluations-by-stage',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function validateKey(key) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluations-check-key?key=' + key,
        headers: {
          'Content-Type': 'application/json'
        }
      };

      return $http(request);
    }

    function getEvaluationsByUser(user) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluations-by-user?userId=' + user.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function getEvaluationsByEvaluator(user) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluations-by-evaluator?evaluatorId=' + user.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function getEvaluation(evaluation) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluation/' + evaluation.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function createEvaluation(evaluation) {
      var postRequest = {
        method: 'POST',
        url: RESOURCE.API_URL + 'evaluations',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: evaluation
      };
      return $http(postRequest);
    }

    function updateEvaluation(evaluation) {
      var postRequest = {
        method: 'PUT',
        url: RESOURCE.API_URL + 'evaluations/' + evaluation.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: evaluation
      };

      return $http(postRequest);
    }

    function updateEvaluationStage(stage) { 
      var postRequest = {
        method: 'PUT',
        url: RESOURCE.API_URL + 'stages/' + stage.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: stage
      };

      return $http(postRequest);
    }

    function changeStateEvaluation(evaluation) {
      var postRequest = {
        method: 'PUT',
        url: RESOURCE.API_URL + 'evaluation/changeState',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: evaluation
      };

      return $http(postRequest);
    }

    function getEvaluationResults(evaluation) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluation-results?' + 'id=' + evaluation.id + '&language=' + sessionStorage.getItem('language'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function getEvaluationResultsCSV(evaluation) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'generate-report?id=' + evaluation.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };
      return $http(request);
    }

    function getCSVReportByUserDateRange(userId, startDate, finalDate) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'user-evaluations-by-date?userId=' + userId + '&initialDate=' + startDate + '&finalDate=' + finalDate,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };
      return $http(request);
    }

    function checkEvaluationCompleted(evaluatorId) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'evaluations-completed?evaluatorId=' + evaluatorId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };
      return $http(request);
    }

    function saveEvaluationToSession(evaluation) {
      sessionStorage.setItem('Evaluation', JSON.stringify(evaluation));
    }

    function getSavedEvaluation() {
      return JSON.parse(sessionStorage.getItem('Evaluation'));
    }

    function clearSessionEvaluation() {
      saveEvaluationToSession(null);
    }
  }

})();
