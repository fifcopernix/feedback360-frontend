(function() {
  'use strict';

  angular
    .module('app.service')
    .service('QuestionService', QuestionService);

  /* @ngInject */
  function QuestionService(UserService, $http, RESOURCE, $base64) {

    this.getQuestionTypes = getQuestionTypes;
    this.createQuestion = createQuestion;
    this.updateQuestion = updateQuestion;

    function getQuestionTypes() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'questionTypes',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function createQuestion(question) {
      var postRequest = {
        method: 'POST',
        url: RESOURCE.API_URL + 'questions',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: question
      };

      return $http(postRequest);
    }

    function updateQuestion(question) {
      var postRequest = {
        method: 'PUT',
        url: RESOURCE.API_URL + 'questions',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: question
      };

      return $http(postRequest);
    }
  }
})();
