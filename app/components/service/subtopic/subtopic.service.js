(function() {
  'use strict';

  angular
    .module('app.service')
    .service('SubtopicService', SubtopicService);

  /* @ngInject */
  function SubtopicService(UserService, $http, RESOURCE, $base64) {

    this.getSubtopics = getSubtopics;
    this.getSubtopic = getSubtopic;
    this.createSubtopic = createSubtopic;
    this.updateSubtopic = updateSubtopic;
    this.changeStateSubtopic = changeStateSubtopic;

    function getSubtopics() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'topic',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function getSubtopic(topic) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'topic/' + topic.id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function createSubtopic(subtopic) {
      var postRequest = {
        method: 'POST',
        url:  RESOURCE.API_URL + 'subtopics',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: subtopic
      };

      return $http(postRequest);
    }

    function updateSubtopic(subtopic) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'subtopics',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: subtopic
      };

      return $http(postRequest);
    }

    function changeStateSubtopic(topic) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'topic/changeState',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: topic
      };

      return $http(postRequest);
    }
  }
})();
