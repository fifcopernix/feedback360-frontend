(function () {
  'use strict';

  angular
    .module('app.service')
    .service('AnswerService', AnswerService);

  /* @ngInject */
  function AnswerService(UserService, $http, RESOURCE, $base64) {
    this.submitAnswers = submitAnswers;
    this.draftAnswers = draftAnswers;
    this.getAnswers = getAnswers;

    function submitAnswers(answers) {
      var requuest = {
        method: 'POST',
        url: RESOURCE.API_URL + 'evaluations-submit',
        headers: {
          'ContentType': 'application/json'
        },
        data: answers
      };

      return $http(requuest);
    }
    function draftAnswers(answers) {
      var requuest = {
        method: 'POST',
        url: RESOURCE.API_URL + 'evaluations-draft',
        headers: {
          'ContentType': 'application/json'
        },
        data: answers
      };

      return $http(requuest);
    }

    function getAnswers(evaluatorId, evaluationId) {
      var requuest = {
        method: 'GET',
        url: RESOURCE.API_URL + 'answers-by-evaluator-and-evaluation?' + 'evaluatorId=' + evaluatorId + '&evaluationId=' + evaluationId,
        headers: {
          'ContentType': 'application/json'
        }
      };

      return $http(requuest);
    }
  }
})();
