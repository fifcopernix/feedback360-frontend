(function() {
  'use strict';

  angular
    .module('app.service')
    .service('SurveyService', SurveyService);

  /* @ngInject */
  function SurveyService(UserService, $http, RESOURCE, $base64, $log) {

    this.getSurveys = getSurveys;
    this.getSurveyById = getSurveyById;
    this.createSurvey = createSurvey;
    this.updateSurvey = updateSurvey;
    this.changeStateSurvey = changeStateSurvey;
    this.deleteSurvey = deleteSurvey;

    function getSurveys() {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + 'surveys',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        }
      };

      return $http(request);
    }

    function getSurveyById(survey) {
      var request = {
        method: 'GET',
        url: RESOURCE.API_URL + `surveys/${survey.id}`,
        headers: {
          'Content-Type': 'application/json'
        }
      };

      return $http(request);
    }

    function createSurvey(survey) {
      var postRequest = {
        method: 'POST',
        url:  RESOURCE.API_URL + 'surveys',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: survey
      };

      return $http(postRequest);
    }

    function updateSurvey(survey) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'surveys',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: survey
      };

      return $http(postRequest);
    }

    function changeStateSurvey(survey) {
      var postRequest = {
        method: 'PUT',
        url:  RESOURCE.API_URL + 'survey/changeState',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
        data: survey
      };

      return $http(postRequest);
    }

    function deleteSurvey(survey) {
      var request = {
        method: 'DELETE',
        url:  RESOURCE.API_URL + `surveys/${survey.id}`,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': UserService.getAuthorization()
        },
      };

      return $http(request);
    }

  }

})();
