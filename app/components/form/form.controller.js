(function () {
  'use strict';

  angular
    .module('app.login')
    .controller('FormController', FormController);

  /* @ngInject */
  function FormController(EvaluationService,
    SurveyService,
    UserService,
    AnswerService,
    ngNotify,
    RESOURCE,
    $stateParams,
    $state,
    $translate) {
    var vm = this;
    var evaluationKey = $stateParams.key;
    vm.answers = [];
    vm.openAnswers = [];
    vm.closeAnswers = [];
    vm.survey = {};
    vm.questions = [];
    vm.evaluation = {};
    vm.scale1 = RESOURCE.SCALE1; // 0 - 1
    vm.scale2 = RESOURCE.SCALE2; // 1.5 - 5
    vm.setLanguage = setLanguage;
    vm.isLanguage = isLanguage;
    vm.sendAnswers = sendAnswers;
    vm.postDraftAnswers = postDraftAnswers;
    vm.draftAnswers = [];
    vm.translate = $translate;
    vm.setAnswersResponse = setAnswersResponse;
    vm.getAnswersResponse = getAnswersResponse;

    window.addEventListener('scroll', showScaleImage);
    window.addEventListener('touchmove', showScaleImage);

    var answerRequest = {
      evaluator: {},
      answers: []
    };

    getEvaluation().then(() => {
      getAnswers().then(() => {
        vm.setAnswersResponse();
      });
    });
    setLanguage('es');

    angular.element(document).ready(function () {
      vm.scaleImage = document.getElementById('scaleImageId');
    });

    function getEvaluation() {
      return EvaluationService.validateKey(evaluationKey)
        .then(function (response) {
          getSurvey(response.data.evaluation.survey, response.data.evaluation);
          answerRequest.evaluator = response.data;
          if (response.data.evaluation.status === 'COMPLETE') {
            vm.validEvaluation = false;
          } else {
            vm.validEvaluation = true;
          }

        })
        .catch(function (response) {
          vm.translate(['EVALUATION_INVALID_KEY']).then(translations => {
            ngNotify.set(translations.EVALUATION_INVALID_KEY, 'error');
          });
        });
    }

    function getSurvey(survey, evaluation) {
      SurveyService.getSurveyById(survey)
        .then(function (response) {
          vm.survey = response.data;
          vm.questions = getQuestionsFromSurvey(vm.survey, evaluation);
          sortAnswersAndQuestions();
        })
        .catch(function (error) {
          vm.translate(['ERROR_LOADING_SURVEY']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_SURVEY, 'error');
          });
        });
    }

    function sortAnswersAndQuestions() {
      vm.answers.forEach(answer => {
        if (answer.type === 'Closed') {
          vm.closeAnswers.push(answer);
        }
        else {
          vm.openAnswers.push(answer);
        }
      });
      vm.closeAnswers.sort(function (a, b) {
        return a.question.id - b.question.id;
      });
      vm.openAnswers.sort(function (a, b) {
        return a.question.id - b.question.id;
      });
      vm.answers = vm.closeAnswers.concat(vm.openAnswers);
    }

    function getAnswers() {
      answerRequest.answers = vm.answers;
      return AnswerService.getAnswers(answerRequest.evaluator.id, answerRequest.evaluator.evaluation.id)
        .then(function (response) {
          vm.draftAnswers = response.data;
        })
        .catch(function (response) {

        })
    }

    function sendAnswers() {
      answerRequest.answers = vm.answers;
      AnswerService.submitAnswers(answerRequest)
        .then(function (response) {
          $state.go('thanks');
          vm.translate(['SUCESS_SENDING_ANSWERS']).then(translations => {
            ngNotify.set(translations.SUCESS_SENDING_ANSWERS, 'success');
          });
        })
        .catch(function (response) {
          vm.translate(['ERROR_SENDING_ANSWERS']).then(translations => {
            ngNotify.set(translations.ERROR_SENDING_ANSWERS, 'error');
          });
        })
    }

    function postDraftAnswers() {
      answerRequest.answers = vm.answers;
      AnswerService.draftAnswers(answerRequest)
        .then(function () {
          $state.go('thanks');
          vm.translate(['SUCESS_SENDING_ANSWERS']).then(translations => {
            ngNotify.set(translations.SUCESS_SENDING_ANSWERS, 'success');
          });
        })
        .catch(function () {
          vm.translate(['ERROR_SENDING_ANSWERS']).then(translations => {
            ngNotify.set(translations.ERROR_SENDING_ANSWERS, 'error');
          });
        })
    }

    function setAnswersResponse() {
      vm.answers.forEach(answer => {
        getAnswersResponse(answer);
      });
    }

    function getAnswersResponse(answer) {
      vm.draftAnswers.forEach(draftAnswer => {
        (answer.question.id === draftAnswer.question.id) ? answer.response = draftAnswer.response : answer.response;
      });
    }

    function getQuestionsFromSurvey(survey, evaluation) {
      var result = [];

      survey.topics.forEach(function (topic) {
        topic.subtopics.forEach(function (subtopic) {
          subtopic.questions.forEach(function (question) {
            var answerTemplate = {
              response: '',
              question: { id: question.id },
              evaluation: { id: evaluation.id },
              type: question.type
            };

            result.push(question);
            vm.answers.push(answerTemplate);
          })
        })
      })
      result.sort(function (a, b) { return (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0); });
      return result;
    }

    function isLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function setLanguage(lang) {
      UserService.setLanguage(lang);
      $translate.use(lang);
    }

    function showScaleImage() {
      switch (true) {
        case screen.width < 500:
          showImageMobile();
          break;
        case screen.width < 1350:
          showImageIpad();
          break;
        default:
          showImagePC()
          break;
      }
    }

    function showImageMobile() {
      if (window.scrollY < 840) {
        vm.scaleImage.style.display = 'none';
      }
      else {
        vm.scaleImage.style.display = 'inline';
      }
    }

    function showImagePC() {
      if (window.scrollY < 596) {
        vm.scaleImage.style.display = 'none';
      }
      else {
        vm.scaleImage.style.display = 'inline';
      }
    }

    function showImageIpad() {
      if (window.scrollY < 700) {
        vm.scaleImage.style.display = 'none';
      }
      else {
        vm.scaleImage.style.display = 'inline';
      }
    }
  }
})();
