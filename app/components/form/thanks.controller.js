(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('ThanksController', ThanksController);

  /* @ngInject */
  function ThanksController(UserService, $translate, $log) {
    var vm = this;
    vm.setLanguage = setLanguage;
    vm.isLanguage = isLanguage;

    function isLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function setLanguage(lang) {
      UserService.setLanguage(lang);
      $translate.use(lang);
    }
  }
})();
