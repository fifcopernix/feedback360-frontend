(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('AdminQuestionController', AdminQuestionController);

  /* @ngInject */
  function AdminQuestionController($state, QuestionService, $uibModal, ngNotify, $translate) {
    var vm = this;
    vm.questions = [];
    vm.createQuestion = createQuestion;
    vm.updateQuestion = updateQuestion;
    vm.deleteQuestion = deleteQuestion;
    vm.translate = $translate;

    activate();
    getQuestions();

    function activate() {
      QuestionService.verifyCredentials();
      if (!QuestionService.isAdmin()) {
        $state.go('home.dashboard');
        vm.translate(['PERMISSION_ERROR']).then(translations => {
          ngNotify.set(translations.PERMISSION_ERROR, 'error');
        });
      }
    }

    function getQuestions() {
      QuestionService.getQuestions()
        .then(function(questionsData) {
          vm.questions = questionsData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_QUESTIONS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_QUESTIONS, 'error');
          });
        });
    }

    function createQuestion() {
      $uibModal.open({
        templateUrl: 'app/components/admin/question/create/create.html',
        controller: 'CreateQuestionController as vm',
        resolve: {
          questions: function() {
            return vm.questions;
          }
        }
      });
    }

    function updateQuestion(question) {
      $uibModal.open({
        templateUrl: 'app/components/admin/question/update/update.html',
        controller: 'UpdateQuestionController as vm',
        resolve: {
          question: function() {
            return question;
          }
        }
      });
    }

    function deleteQuestion(question) {
      $uibModal.open({
        templateUrl: 'app/components/admin/question/delete/delete.html',
        controller: 'DeleteQuestionController as vm',
        resolve: {
          question: function() {
            return question;
          }
        }
      });
    }

  }

})();
