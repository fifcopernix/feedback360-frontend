(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('UpdateQuestionController', UpdateQuestionController);

  /* @ngInject */
  function UpdateQuestionController(QuestionService,
                                    question,
                                    subtopic,
                                    questionTypes,
                                    $uibModalInstance,
                                    ngNotify,
                                    $translate) {
    var vm = this;
    vm.translate = $translate;
    vm.questionTypes = questionTypes;
    vm.question = question;
    vm.subtopic = subtopic;
    vm.updateQuestion = updateQuestion;
    vm.close = close;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function updateQuestion() {
      var updateQuestionRequest = {
        id: vm.question.id,
        descriptionEs: vm.question.statementEs,
        descriptionEn: vm.question.statementEn,
        type:{
          id: questionTypes[0].name == vm.question.type ? questionTypes[0].id : questionTypes[1].id
        },
        subtopic:{
          id: subtopic.id
        }
      };

      QuestionService.updateQuestion(updateQuestionRequest)
        .then(function(response) {
          vm.question = {};
          vm.translate(['QUESTION_UPDATED']).then(translations => {
            ngNotify.set(translations.QUESTION_UPDATED, 'success');
          });
        })
        .catch(function(error) {
          vm.question = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
