(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('DeleteQuestionController', DeleteQuestionController);

  /* @ngInject */
  function DeleteQuestionController(QuestionService,
                                    question,
                                    $uibModalInstance,
                                    ngNotify,
                                    $translate) {
    var vm = this;
    vm.close = close;
    vm.translate = $translate;
    vm.question = question;
    vm.deleteQuestion = deleteQuestion;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function deleteQuestion() {
      QuestionService.changeStateQuestion(vm.question)
        .then(function(data) {
          vm.question.active = !vm.question.active;
          vm.translate(['QUESTION_DELETED']).then(translations => {
            ngNotify.set(translations.QUESTION_DELETED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
