(function() {
  'use strict';

  angular
    .module('app.admin')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home.question', {
        url: '/admin/question',
        templateUrl: 'app/components/admin/question/admin-question.html',
        controller: 'AdminQuestionController as vm'
      });
  }
})();
