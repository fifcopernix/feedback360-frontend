(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('CreateQuestionController', CreateQuestionController);

  /* @ngInject */
  function CreateQuestionController(ManagerService,
                                    QuestionService,
                                    subtopic,
                                    questionTypes,
                                    $uibModalInstance,
                                    ngNotify,
                                    $translate) {

    var vm = this;
    vm.subtopic = subtopic;
    vm.translate = $translate;
    vm.questionTypes = [];
    vm.createQuestion = createQuestion;
    vm.close = close;

    vm.question = {
      descriptionEs: '',
      descriptionEn: '',
      type: {
        id: ''
      },
      subtopic: {
        id: subtopic.id
      }
    };

    getQuestionTypes();

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function getQuestionTypes() {
      QuestionService.getQuestionTypes()
        .then(function(response) {
          vm.questionTypes = response.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_QUESTION_TYPES']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_QUESTION_TYPES, 'error');
          });
        });
    }

    function createQuestion() {
      QuestionService.createQuestion(vm.question)
        .then(function(response) {
          var newQuestion = {
            id: response.data.id,
            statementEn: response.data.descriptionEn,
            statementEs: response.data.descriptionEs,
            type: response.data.type.name
          };

          vm.subtopic.questions.push(newQuestion);
          vm.translate(['QUESTION_ADDED']).then(translations => {
            ngNotify.set(translations.QUESTION_ADDED, 'success');
          });
        })
        .catch(function(error) {
          vm.question = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
