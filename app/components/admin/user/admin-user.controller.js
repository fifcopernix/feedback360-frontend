(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('AdminUserController', AdminUserController);

  /* @ngInject */
  function AdminUserController($state, UserService, $uibModal, ngNotify, $translate) {
    var vm = this;
    vm.users = [];
    vm.createUser = createUser;
    vm.updateUser = updateUser;
    vm.deleteUser = deleteUser;
    vm.translate = $translate;

    activate();
    getUsers();

    function activate() {
      UserService.verifyCredentials();
      if (!UserService.isAdmin()) {
        $state.go('home.dashboard');
        vm.translate(['PERMISSION_ERROR']).then(translations => {
          ngNotify.set(translations.PERMISSION_ERROR, 'error');
        });
      }
    }

    function getUsers() {
      UserService.getUsers()
        .then(function(usersData) {
          vm.users = usersData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_USERS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_USERS, 'error');
          });
        });
    }

    function createUser() {
      $uibModal.open({
        templateUrl: 'app/components/admin/user/create/create.html',
        controller: 'CreateUserController as vm',
        resolve: {
          users: function() {
            return vm.users;
          }
        }
      });
    }

    function updateUser(user) {
      $uibModal.open({
        templateUrl: 'app/components/admin/user/update/update.html',
        controller: 'UpdateUserController as vm',
        resolve: {
          user: function() {
            return user;
          }
        }
      });
    }

    function deleteUser(user) {
      $uibModal.open({
        templateUrl: 'app/components/admin/user/delete/delete.html',
        controller: 'DeleteUserController as vm',
        resolve: {
          user: function() {
            return user;
          }
        }
      });
    }

  }

})();
