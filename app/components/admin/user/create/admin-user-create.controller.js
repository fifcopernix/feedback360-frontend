(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('CreateUserController', CreateUserController);

  /* @ngInject */
  function CreateUserController(ManagerService,
                                UserService,
                                users,
                                $uibModalInstance,
                                ngNotify,
                                $translate) {
    var vm = this;
    vm.managers = [];
    vm.manager = {};
    vm.userTypes = [];
    vm.userType = {};
    vm.user = {};
    vm.users = users;
    vm.createUser = createUser;
    vm.close = close;
    vm.translate = $translate;

    getManagers();
    getUserTypes();

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function getManagers() {
      ManagerService.getManagers()
        .then(function(managersData) {
          vm.managers = managersData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_MANAGERS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_MANAGERS, 'error');
          });
        });
    }

    function getUserTypes() {
      UserService.getUserTypes()
        .then(function(userTypesData) {
          vm.userTypes = userTypesData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_USERS_TYPES']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_USERS_TYPES, 'error');
          });
        });
    }

    function createUser() {
      vm.user.active = true;
      vm.user.manager = JSON.parse(vm.manager);
      vm.user.userType = JSON.parse(vm.userType);
      UserService.createUser(vm.user)
        .then(function(data) {
          vm.users.push(vm.user);
          vm.user = {};
          vm.translate(['USER_ADDED']).then(translations => {
            ngNotify.set(translations.USER_ADDED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
