(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('UpdateUserController', UpdateUserController);

  /* @ngInject */
  function UpdateUserController(ManagerService,
                                UserService,
                                user,
                                $uibModalInstance,
                                ngNotify,
                                $translate) {
    var vm = this;
    vm.managers = [];
    vm.manager = {};
    vm.userTypes = [];
    vm.userType = {};
    vm.user = user;
    vm.updateUser = updateUser;
    vm.close = close;
    vm.translate = $translate;

    getManagers();
    getUserTypes();

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function getManagers() {
      ManagerService.getManagers()
        .then(function(managersData) {
          vm.managers = managersData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_MANAGERS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_MANAGERS, 'error');
          });
        });
    }

    function getUserTypes() {
      UserService.getUserTypes()
        .then(function(userTypesData) {
          vm.userTypes = userTypesData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_USERS_TYPES']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_USERS_TYPES, 'error');
          });
        });
    }

    function updateUser() {
      vm.user.manager = JSON.parse(vm.manager);
      vm.user.userType = JSON.parse(vm.userType);
      UserService.updateUser(vm.user)
        .then(function(data) {
          vm.user = {};
          vm.translate(['USER_UPDATED']).then(translations => {
            ngNotify.set(translations.USER_UPDATED, 'success');
          });
        })
        .catch(function(error) {
          vm.user = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
