(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('DeleteUserController', DeleteUserController);

  /* @ngInject */
  function DeleteUserController(UserService,
                                user,
                                $uibModalInstance,
                                ngNotify,
                                $translate) {
    var vm = this;
    vm.close = close;
    vm.user = user;
    vm.deleteUser = deleteUser;
    vm.translate = $translate;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function deleteUser() {
      UserService.changeStateUser(vm.user)
        .then(function(data) {
          vm.user.active = !vm.user.active;
          vm.translate(['USER_UPDATED']).then(translations => {
            ngNotify.set(translations.USER_UPDATED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
