(function() {
    'use strict';
  
    angular
      .module('app.admin')
      .controller('DeleteSurveyController', DeleteSurveyController);
  
    /* @ngInject */
    function DeleteSurveyController(SurveyService,
                                    survey,
                                    surveys,
                                    $uibModalInstance,
                                    ngNotify,
                                    $translate) {
      var vm = this;
      vm.survey = survey;
      vm.deleteSurvey = deleteSurvey;
      vm.close = close;
      vm.translate = $translate;
      vm.surveys =surveys;
  
      function close() {
        $uibModalInstance.dismiss('cancel');
      }

      function refreshSurveys(id){
        vm.surveys.forEach(survey =>{
            if(survey.id===id)
            vm.surveys.splice(vm.surveys.indexOf(survey),1);
        })

      }
  
      function deleteSurvey() {
        refreshSurveys(vm.survey.id);
        SurveyService.deleteSurvey(vm.survey)
          .then(function() {
            vm.translate(['SURVEY_DELETED']).then(translations => {
              ngNotify.set(translations.SURVEY_DELETED, 'success');
            });
          })
          .catch(function(error) {
            vm.survey = {};
            vm.translate(['ERROR']).then(translations => {
              ngNotify.set(translations.ERROR, 'error');
            });
          });
        close();
      }
    }
  })();