(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('DetailsSurveyController', DetailsSurveyController);

  /* @ngInject */
  function DetailsSurveyController(SurveyService,
                                  UserService,
                                  QuestionService,
                                  survey,
                                  $uibModalInstance,
                                  $uibModal,
                                  ngNotify,
                                  $state,
                                  $translate) {

    var vm = this;
    vm.survey = survey
    vm.close = close;
    vm.updateSurvey = updateSurvey;
    vm.addTopic = addTopic;
    vm.updateTopic = updateTopic;
    vm.addSubtopic = addSubtopic;
    vm.addQuestion = addQuestion;
    vm.questionTypes = [];
    vm.currentLanguage = currentLanguage;
    vm.translate = $translate;

    getQuestionTypes();

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function getQuestionTypes() {
      QuestionService.getQuestionTypes()
        .then(function(response) {
          vm.questionTypes = response.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_QUESTION_TYPES']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_QUESTION_TYPES, 'error');
          });
        });
    }

    function updateSurvey(survey) {
      $uibModal.open({
        templateUrl: 'app/components/admin/survey/update/update.html',
        controller: 'UpdateSurveyController as vm',
        resolve: {
          survey: function() {
            return survey;
          }
        }
      });
    }

    function addTopic(survey) {
      $uibModal.open({
        templateUrl: 'app/components/admin/topic/create/create.html',
        controller: 'CreateTopicController as vm',
        resolve: {
          survey: function() {
            return survey;
          }
        }
      });
    }

    function updateTopic(topic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/topic/update/update.html',
        controller: 'UpdateTopicController as vm',
        resolve: {
          topic: function() {
            return topic;
          }
        }
      });
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function addSubtopic(topic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/subtopic/create/create.html',
        controller: 'CreateSubtopicController as vm',
        resolve: {
          topic: function() {
            return topic;
          }
        }
      });
    }

    function addQuestion(subtopic, questionTypes) {
      $uibModal.open({
        templateUrl: 'app/components/admin/question/create/create.html',
        controller: 'CreateQuestionController as vm',
        resolve: {
          subtopic: function() {
            return subtopic;
          },
          questionTypes: function() {
            return questionTypes;
          }
        }
      });
    }
  }
})();
