(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('UpdateSurveyController', UpdateSurveyController);

  /* @ngInject */
  function UpdateSurveyController(SurveyService,
                                  survey,
                                  $uibModalInstance,
                                  ngNotify,
                                  $state,
                                  $translate) {
    var vm = this;
    vm.survey = survey;
    vm.close = close;
    vm.updateSurvey = updateSurvey;
    vm.translate = $translate;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function updateSurvey() {
      SurveyService.updateSurvey(vm.survey)
        .then(function(data) {
          vm.translate(['SURVEY_UPDATED']).then(translations => {
            ngNotify.set(translations.SURVEY_UPDATED, 'success');
          });
        })
        .catch(function(error) {
          vm.survey = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
