(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('CreateSurveyController', CreateSurveyController);

  /* @ngInject */
  function CreateSurveyController(SurveyService,
                                  UserService,
                                  surveys,
                                  $uibModalInstance,
                                  ngNotify,
                                  $state,
                                  $translate) {
    var vm = this;

    vm.survey = {
      id: '',
      nameEs: '',
      nameEn: '',
      topics: []
    };

    vm.surveys = surveys;
    vm.createSurvey = createSurvey;
    vm.close = close;
    vm.translate = $translate;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function createSurvey() {
      SurveyService.createSurvey(vm.survey)
        .then(function(resp) {
          vm.survey.id = resp.data.id;
          vm.surveys.push(vm.survey);
          vm.translate(['CREATE_SURVEY']).then(translations => {
            ngNotify.set(translations.CREATE_SURVEY, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
