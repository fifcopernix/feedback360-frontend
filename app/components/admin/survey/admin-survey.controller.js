(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('AdminSurveyController', AdminSurveyController);

  /* @ngInject */
  function AdminSurveyController($state, UserService, SurveyService, QuestionService, $uibModal, ngNotify, $translate) {
    var vm = this;
    vm.surveys = [];
    vm.createSurvey = createSurvey;
    vm.currentLanguage = currentLanguage;
    vm.updateSurvey = updateSurvey;
    vm.deleteSurvey = deleteSurvey;
    vm.addTopic = addTopic;
    vm.updateTopic = updateTopic;
    vm.addSubtopic = addSubtopic;
    vm.updateSubtopic = updateSubtopic;
    vm.addQuestion = addQuestion;
    vm.updateQuestion = updateQuestion;
    vm.questionTypes = [];
    vm.isQuestionOpen = isQuestionOpen;
    vm.translate = $translate;

    activate();
    getSurveys();
    getQuestionTypes();

    function activate() {
      UserService.verifyCredentials();
      if (!UserService.isAdmin()) {
        $state.go('home.dashboard');
        vm.translate(['PERMISSION_ERROR']).then(translations => {
          ngNotify.set(translations.PERMISSION_ERROR, 'error');
        });
      }
    }

    function getSurveys() {
      SurveyService.getSurveys()
        .then(function(surveysData) {
          vm.surveys = surveysData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_SURVEYS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_SURVEYS, 'error');
          });
        });
    }

    function createSurvey() {
      $uibModal.open({
        templateUrl: 'app/components/admin/survey/create/create.html',
        controller: 'CreateSurveyController as vm',
        resolve: {
          surveys: function() {
            return vm.surveys;
          }
        }
      });
    }

    function getQuestionTypes() {
      QuestionService.getQuestionTypes()
        .then(function(response) {
          vm.questionTypes = response.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_QUESTION_TYPES']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_QUESTION_TYPES, 'error');
          });
        });
    }

    function updateSurvey(survey) {
      $uibModal.open({
        templateUrl: 'app/components/admin/survey/update/update.html',
        controller: 'UpdateSurveyController as vm',
        resolve: {
          survey: function() {
            return survey;
          }
        }
      });
    }

    function deleteSurvey(survey,surveys){
      $uibModal.open({
        templateUrl: 'app/components/admin/survey/delete/delete.html',
        controller: 'DeleteSurveyController as vm',
        resolve: {
          survey: function() {
            return survey;
          },
          surveys: function(){
            return surveys;

          }
        }
      });

    }

    function addTopic(survey) {
      $uibModal.open({
        templateUrl: 'app/components/admin/topic/create/create.html',
        controller: 'CreateTopicController as vm',
        resolve: {
          survey: function() {
            return survey;
          }
        }
      });
    }

    function updateTopic(topic, survey) {
      $uibModal.open({
        templateUrl: 'app/components/admin/topic/update/update.html',
        controller: 'UpdateTopicController as vm',
        resolve: {
          topic: function() {
            return topic;
          },
          survey: function() {
            return survey;
          }
        }
      });
    }

    function addSubtopic(topic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/subtopic/create/create.html',
        controller: 'CreateSubtopicController as vm',
        resolve: {
          topic: function() {
            return topic;
          }
        }
      });
    }

    function updateSubtopic(subtopic, topic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/subtopic/update/update.html',
        controller: 'UpdateSubtopicController as vm',
        resolve: {
          subtopic: function() {
            return subtopic;
          },
          topic: function() {
            return topic;
          }
        }
      });
    }

    function addQuestion(subtopic, questionTypes) {
      $uibModal.open({
        templateUrl: 'app/components/admin/question/create/create.html',
        controller: 'CreateQuestionController as vm',
        resolve: {
          subtopic: function() {
            return subtopic;
          },
          questionTypes: function() {
            return questionTypes;
          }
        }
      });
    }

    function updateQuestion(question, subtopic, questionTypes) {
      $uibModal.open({
        templateUrl: 'app/components/admin/question/update/update.html',
        controller: 'UpdateQuestionController as vm',
        resolve: {
          question: function() {
            return question;
          },
          subtopic: function() {
            return subtopic;
          },
          questionTypes: function() {
            return questionTypes;
          }
        }
      });
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function isQuestionOpen(qType) {
      return qType == 'Open';
    }
  }
})();
