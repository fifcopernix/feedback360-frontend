(function () {
  'use strict';

  angular
    .module('app.admin')
    .controller('UpdateEvaluationController', UpdateEvaluationController);

  /* @ngInject */
  function UpdateEvaluationController(UserService,
    OnTrackService,
    EvaluationService,
    stage,
    $uibModalInstance,
    $filter,
    RESOURCE,
    ngNotify,
    $state,
    $translate) {
    var vm = this;
    vm.stage = stage;
    vm.translate = $translate;
    vm.onTrackList = [];
    vm.closeModal = closeModal;
    vm.user = UserService.getCurrentUser();
    vm.updateEvaluation = updateEvaluation;

    vm.toggleStartDatePopup = toggleStartDatePopup;
    vm.toggleEndDatePopup = toggleEndDatePopup;

    vm.dateOptions = {
      showWeeks: false,
      datepickerMode: 'year'
    };
    vm.startDatePopup = {
      opened: false
    };
    vm.endDatePopup = {
      opened: false
    };
    vm.currentLanguage = currentLanguage;

    function closeModal() {
      $uibModalInstance.dismiss('cancel');
    }

    function updateEvaluation() {
      EvaluationService.updateEvaluationStage(vm.stage)
        .then(function (response) {
          vm.translate(['EVALUATION_UPDATED']).then(translations => {
            ngNotify.set(translations.EVALUATION_UPDATED, 'success');
          });
        })
        .catch(function (error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      closeModal();
    }

    function toggleStartDatePopup() {
      vm.startDatePopup.opened = true;
      return vm.startDatePopup.opened;
    }

    function toggleEndDatePopup() {
      vm.endDatePopup.opened = true;
      return vm.endDatePopup.opened;
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }
  }
})();
