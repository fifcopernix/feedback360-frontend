(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('DeleteEvaluationController', DeleteEvaluationController);

  /* @ngInject */
  function DeleteEvaluationController(EvaluationService,
                                      evaluation,
                                      $uibModalInstance,
                                      ngNotify,
                                      $translate) {
    var vm = this;
    vm.evaluation = evaluation;
    vm.translate = $translate;
    vm.closeModal = closeModal;
    vm.deleteEvaluation = deleteEvaluation;

    function closeModal() {
      $uibModalInstance.dismiss('cancel');
    }

    function deleteEvaluation() {
      EvaluationService.changeStateEvaluation(vm.evaluation)
        .then(function(data) {
          vm.evaluation.active = !vm.evaluation.active;
          vm.translate(['EVALUATION_DELETED']).then(translations => {
            ngNotify.set(translations.EVALUATION_DELETED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      closeModal();
    }

  }

})();
