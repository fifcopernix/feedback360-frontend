(function() {
  'use strict';

  angular
    .module('app.admin')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home.adminEvaluation', {
        url: '/admin/evaluation',
        templateUrl: 'app/components/admin/evaluation/admin-evaluation.html',
        controller: 'AdminEvaluationController as vm'
      });
  }
})();
