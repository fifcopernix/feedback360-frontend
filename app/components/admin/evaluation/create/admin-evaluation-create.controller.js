(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('CreateEvaluationController', CreateEvaluationController);

  /* @ngInject */
  function CreateEvaluationController(UserService,
                                      OnTrackService,
                                      EvaluationService,
                                      stages,
                                      surveys,
                                      $uibModalInstance,
                                      $filter,
                                      RESOURCE,
                                      ngNotify,
                                      $state,
                                      $translate) {
    var vm = this;
    vm.stages = stages;
    vm.surveys = surveys;
    vm.translate = $translate;
    vm.onTrackList = [];
    vm.evaluation = {
      emails: [],
      survey: {
        id: ''
      },
      stage: {
        nameEn: '',
        nameEs: '',
        startDate: '',
        endDate: '',
        evaluations: []
      },
      language: null
    };
    vm.closeModal = closeModal;
    vm.onTrack = {};
    vm.user = UserService.getCurrentUser();
    vm.createEvaluation = createEvaluation;

    vm.toggleStartDatePopup = toggleStartDatePopup;
    vm.toggleEndDatePopup = toggleEndDatePopup;
    vm.formatUserEmails = formatUserEmails;

    // datepicker settings
    vm.dateOptions = {
      showWeeks: false,
      datepickerMode: 'year',
      minDate: new Date(),
    };
    vm.startDatePopup = {
      opened: false
    };
    vm.endDatePopup = {
      opened: false
    };
    vm.currentLanguage = currentLanguage;

    function closeModal() {
      $uibModalInstance.dismiss('cancel');
    }

    function formatUserEmails() {
      for (var i = 0; i < vm.evaluation.emails.length; i++) { 
        vm.evaluation.emails[i] = vm.evaluation.emails[i].toLowerCase();
      }
    }

    function createEvaluation() {
      vm.evaluation.language = UserService.getLanguage();
      formatUserEmails();
      EvaluationService.createEvaluation(vm.evaluation)
        .then(function(response) {
          let stage = vm.evaluation.stage;
          stage.evaluations = response.data;
          vm.stages.push(stage);
          vm.evaluation = {};
          vm.translate(['EVALUATION_ADDED']).then(translations => {
            ngNotify.set(translations.EVALUATION_ADDED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      closeModal();
      $state.reload();
    }

    function toggleStartDatePopup() {
      vm.startDatePopup.opened = true;
      return vm.startDatePopup.opened;
    }

    function toggleEndDatePopup() {
      vm.endDateOptions = {
        showWeeks: false,
        datepickerMode: 'year',
        minDate: new Date(vm.evaluation.stage.startDate),
      };
      vm.endDatePopup.opened = true;
      return vm.endDatePopup.opened;
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }
  }
})();
