(function () {
  'use strict';

  angular
    .module('app.admin')
    .controller('AdminEvaluationController', AdminEvaluationController);

  /* @ngInject */
  function AdminEvaluationController(UserService, EvaluationService, $uibModal, SurveyService, ngNotify, $state, $log, $translate) {
    var vm = this;
    vm.stages = [];
    vm.surveys = [];
    vm.createEvaluation = createEvaluation;
    vm.updateEvaluation = updateEvaluation;
    vm.isAdmin = UserService.isAdmin();
    vm.user = UserService.getCurrentUser();
    vm.completedEvaluation = completedEvaluation;
    vm.currentLanguage = currentLanguage;
    vm.generateCSVreport = generateCSVreport;
    vm.generateCSVReportByUserDateRange = generateCSVReportByUserDateRange;
    vm.toggleStartDatePopup = toggleStartDatePopup;
    vm.toggleEndDatePopup = toggleEndDatePopup;
    vm.translate = $translate;

    // datepicker settings
    vm.dateOptions = {
      showWeeks: false,
      datepickerMode: 'year'
    };
    vm.startDatePopup = {
      opened: false
    };
    vm.endDatePopup = {
      opened: false
    };

    getEvaluations();
    getSurveys();

    function getEvaluations() {
      EvaluationService.getEvaluationsByStage()
        .then(function (response) {
          vm.stages = response.data;
        })
        .catch(function (error) {
          vm.translate(['ERROR_LOADING_EVALUATIONS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_EVALUATIONS, 'error');
          });
        });
    }

    function getSurveys() {
      SurveyService.getSurveys()
        .then(function (response) {
          vm.surveys = response.data;
        })
        .catch(function (error) {
          vm.translate(['ERROR_LOADING_SURVEYS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_SURVEYS, 'error');
          });
        });
    }

    function getEvaluationsByUser(user) {
      EvaluationService.getEvaluationsByUser(vm.user)
        .then(function (response) {
          vm.evaluations = response.data;
        })
        .catch(function (error) {
          vm.translate(['ERROR_LOADING_EVALUATION']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_EVALUATION, 'error');
          });
        });
    }

    function createEvaluation() {
      $uibModal.open({
        templateUrl: 'app/components/admin/evaluation/create/create.html',
        controller: 'CreateEvaluationController as vm',
        resolve: {
          stages: function () {
            return vm.stages;
          },
          surveys: function () {
            return vm.surveys;
          }
        }
      }).closed.then(function () {
        $state.reload();
      });
    }

    function updateEvaluation(stage) {
      $uibModal.open({
        templateUrl: 'app/components/admin/evaluation/update/update.html',
        controller: 'UpdateEvaluationController as vm',
        resolve: {
          stage: function () {
            return stage;
          }
        }
      });
    }

    function completedEvaluation(ev) {
      if (ev.status == 'COMPLETED') {
        return 'completed-evaluation';
      } else {
        return 'pending-evaluation';
      }
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function generateCSVReportByUserDateRange(evaluation, startDate, endDate) {
      EvaluationService.getCSVReportByUserDateRange(evaluation.user.id, startDate, endDate).then(function (response) {
        var uri = 'data:text/csv;charset=utf-8,' + escape(response.data);
        var link = document.createElement("a");
        link.href = uri;
        link.style = "visibility:hidden";
        link.download = evaluation.user.name + '_' + evaluation.stage.nameEs + '_' + startDate + '_' + endDate + '_report.csv';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        vm.translate(['REPORT_GENERATE']).then(translations => {
          ngNotify.set(translations.REPORT_GENERATE, 'success');
        });
      })
        .catch(function (error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        })
    }

    function generateCSVreport(evaluation) {
      EvaluationService.getEvaluationResultsCSV(evaluation)
        .then(function (response) {
          $log.info(response);
          var uri = 'data:text/csv;charset=utf-8,' + escape(response.data);
          var link = document.createElement("a");
          link.href = uri;
          link.style = "visibility:hidden";
          link.download = evaluation.user.name + '_' + evaluation.stage.nameEs + '_' + startDate + '_' + endDate + '_report.csv';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
          vm.translate(['REPORT_GENERATED']).then(translations => {
            ngNotify.set(translations.REPORT_GENERATED, 'success');
          });
        })
        .catch(function (error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        })
    }

    function toggleStartDatePopup() {
      vm.startDatePopup.opened = true;
      return vm.startDatePopup.opened;
    }

    function toggleEndDatePopup() {
      vm.endDatePopup.opened = true;
      return vm.endDatePopup.opened;
    }
  }
})();
