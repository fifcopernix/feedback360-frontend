(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('CreateManagerController', CreateManagerController);

  /* @ngInject */
  function CreateManagerController(ManagerService,
                                  CompanyService,
                                  managers,
                                  $uibModalInstance,
                                  ngNotify,
                                  $translate) {
    var vm = this;
    vm.managers = managers;
    vm.translate = $translate;
    vm.company = {};
    vm.companies = [];
    vm.close = close;
    vm.createManager = createManager;

    getCompanies();

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function getCompanies() {
      CompanyService.getCompanies()
        .then(function(companiesData) {
          vm.companies = companiesData.data;
        })
        .catch(function(error) {
          vm.companies = [];
          vm.translate(['ERROR_LOADING_COMPANIES']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_COMPANIES, 'error');
          });
        });
    }

    function createManager() {
      vm.manager.active = true;
      vm.manager.company = JSON.parse(vm.company);
      ManagerService.createManager(vm.manager)
        .then(function(data) {
          vm.managers.push(vm.manager);
          vm.manager = {};
          vm.translate(['MANAGER_ADDED']).then(translations => {
            ngNotify.set(translations.MANAGER_ADDED, 'success');
          });
        })
        .catch(function(error) {
          vm.manager = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
