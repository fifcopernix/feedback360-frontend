(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('UpdateManagerController', UpdateManagerController);

  /* @ngInject */
  function UpdateManagerController(ManagerService,
                                  CompanyService,
                                  ObjectService,
                                  $uibModalInstance,
                                  manager,
                                  ngNotify,
                                  $translate) {
    var vm = this;
    vm.manager = manager;
    vm.company = manager.company;
    vm.companies = [];
    vm.close = close;
    vm.updateManager = updateManager;
    vm.translate = $translate;

    getCompanies();

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function getCompanies() {
      CompanyService.getCompanies()
        .then(function(companiesData) {
          vm.companies = companiesData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_COMPANIES']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_COMPANIES, 'error');
          });
        });
    }

    function updateManager() {
      vm.manager.company = ObjectService.parseObject(vm.manager.company);
      ManagerService.updateManager(vm.manager)
        .then(function(data) {
          vm.manager = {};
          vm.translate(['MANAGER_UPDATED']).then(translations => {
            ngNotify.set(translations.MANAGER_UPDATED, 'success');
          });
        })
        .catch(function(error) {
          vm.manager = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
