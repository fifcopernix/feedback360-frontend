(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('DeleteManagerController', DeleteManagerController);

  /* @ngInject */
  function DeleteManagerController(ManagerService,
                                  manager,
                                  $uibModalInstance,
                                  ngNotify,
                                  $translate) {
    var vm = this;
    vm.manager = manager;
    vm.close = close;
    vm.deleteManager = deleteManager;
    vm.translate = $translate;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function deleteManager() {
      ManagerService.changeStateManager(vm.manager)
        .then(function(data) {
          vm.manager.active = !vm.manager.active;
          vm.translate(['MANAGER_DELETED']).then(translations => {
            ngNotify.set(translations.MANAGER_DELETED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
