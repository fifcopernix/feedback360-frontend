(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('CreateSubtopicController', CreateSubtopicController);

  /* @ngInject */
  function CreateSubtopicController(SubtopicService,
                                    TopicService,
                                    topic,
                                    $uibModalInstance,
                                    ngNotify,
                                    $uibModalStack,
                                    $translate) {

    var vm = this;
    vm.topic = topic;
    vm.close = close;
    vm.translate = $translate;
    vm.createSubtopic = createSubtopic;

    vm.subtopic = {
      id: 0,
      nameEs: '',
      nameEn: '',
      topic: {
        id: topic.id
      },
      questions: []
    };

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function createSubtopic() {
      SubtopicService.createSubtopic(vm.subtopic)
        .then(function(response) {
          var newSubtopic = {
            id: response.data.id,
            nameEs: response.data.nameEs,
            nameEn: response.data.nameEn,
            topic: response.data.topic.id,
            questions: response.data.questions
          };
          if (!newSubtopic.questions) {
            newSubtopic.questions=[];
          }
          vm.topic.subtopics.push(newSubtopic);
          vm.translate(['CREATED_SUBTOPIC']).then(translations => {
            ngNotify.set(translations.CREATED_SUBTOPIC, 'success');
          });
        })
        .catch(function(error) {
          vm.subtopic = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
