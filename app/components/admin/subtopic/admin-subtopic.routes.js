(function() {
  'use strict';

  angular
    .module('app.admin')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home.subtopic', {
        url: '/admin/subtopic',
        templateUrl: 'app/components/admin/subtopic/admin-subtopic.html',
        controller: 'AdminSubtopicController as vm'
      });
  }
})();
