(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('DeleteSubtopicController', DeleteSubtopicController);

  /* @ngInject */
  function DeleteSubtopicController(SubtopicService,
                                    subtopic,
                                    $uibModalInstance,
                                    ngNotify,
                                    $translate) {
    var vm = this;
    vm.subtopic = subtopic;
    vm.close = close;
    vm.deleteSubtopic = deleteSubtopic;
    vm.translate = $translate;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function deleteSubtopic() {
      SubtopicService.changeStateSubtopic(vm.subtopic)
        .then(function(data) {
          vm.subtopic.active = !vm.subtopic.active;
          vm.translate(['DELETED_SUBTOPIC']).then(translations => {
            ngNotify.set(translations.DELETED_SUBTOPIC, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
