(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('UpdateSubtopicController', UpdateSubtopicController);

  /* @ngInject */
  function UpdateSubtopicController(SubtopicService,
                                    $uibModalInstance,
                                    subtopic,
                                    topic,
                                    ngNotify,
                                    $translate) {
    var vm = this;
    vm.subtopic = subtopic;
    vm.close = close;
    vm.translate = $translate;
    vm.updateSubtopic = updateSubtopic;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function updateSubtopic() {
      var updateSubtopicRequest = {
        id: vm.subtopic.id,
        nameEs: vm.subtopic.nameEs,
        nameEn: vm.subtopic.nameEn,
        topic: {
            id: topic.id
        }
      };

      SubtopicService.updateSubtopic(updateSubtopicRequest)
        .then(function(data) {
          vm.subtopic = {};
          vm.translate(['UPDATED_SUBTOPIC']).then(translations => {
            ngNotify.set(translations.UPDATED_SUBTOPIC, 'success');
          });
        })
        .catch(function(error) {
          vm.subtopic = {};
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
