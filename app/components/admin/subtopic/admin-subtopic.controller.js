(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('AdminSubtopicController', AdminSubtopicController);

  /* @ngInject */
  function AdminSubtopicController($state, UserService, SubtopicService, $uibModal, ngNotify, $translate) {
    var vm = this;
    vm.subtopics = [];
    vm.createSubtopic = createSubtopic;
    vm.updateSubtopic = updateSubtopic;
    vm.deleteSubtopic = deleteSubtopic;
    vm.translate = $translate;

    activate();
    getSubtopics();

    function activate() {
      UserService.verifyCredentials();
      if (!UserService.isAdmin()) {
        $state.go('home.dashboard');
        vm.translate(['PERMISSION_ERROR']).then(translations => {
          ngNotify.set(translations.PERMISSION_ERROR, 'error');
        });
      }
    }

    function getSubtopics() {
      SubtopicService.getSubtopics()
        .then(function(subtopicsData) {
          vm.subtopics = subtopicsData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_SUBTOPICS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_SUBTOPICS, 'error');
          });
        });
    }

    function createSubtopic() {
      $uibModal.open({
        templateUrl: 'app/components/admin/subtopic/create/create.html',
        controller: 'CreateSubtopicController as vm',
        resolve: {
          subtopics: function() {
            return vm.subtopics;
          }
        }
      });
    }

    function updateSubtopic(subtopic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/subtopic/update/update.html',
        controller: 'UpdateSubtopicController as vm',
        resolve: {
          subtopic: function() {
            return subtopic;
          }
        }
      });
    }

    function deleteSubtopic(subtopic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/subtopic/delete/delete.html',
        controller: 'DeleteSubtopicController as vm',
        resolve: {
          subtopic: function() {
            return subtopic;
          }
        }
      });
    }

  }

})();
