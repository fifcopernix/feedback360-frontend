(function() {
  'use strict';

  angular
    .module('app.admin')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home.topic', {
        url: '/admin/topic',
        templateUrl: 'app/components/admin/topic/admin-topic.html',
        controller: 'AdminTopicController as vm'
      });
  }
})();
