(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('UpdateTopicController', UpdateTopicController);

  /* @ngInject */
  function UpdateTopicController(TopicService, $uibModalInstance, topic, survey, ngNotify, $translate) {
    var vm = this;
    vm.close = close;
    vm.topic = topic;
    vm.survey = survey;
    vm.updateTopic = updateTopic;
    vm.translate = $translate;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function updateTopic() {
      var updateTopicRequest = {
        id: vm.topic.id,
        nameEs: vm.topic.nameES,
        nameEn: vm.topic.nameEN,
        survey: {
          id: survey.id
        }
      };

      TopicService.updateTopic(updateTopicRequest)
        .then(function(data) {
          vm.topic = {};
          vm.translate(['TOPIC_UPDATED']).then(translations => {
            ngNotify.set(translations.TOPIC_UPDATED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
