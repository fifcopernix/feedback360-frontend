(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('DeleteTopicController', DeleteTopicController);

  /* @ngInject */
  function DeleteTopicController(TopicService, $uibModalInstance, topic, ngNotify, $translate) {
    var vm = this;
    vm.close = close;
    vm.topic = topic;
    vm.deleteTopic = deleteTopic;
    vm.translate = $translate;

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function deleteTopic() {
      TopicService.changeStateTopic(vm.topic)
        .then(function(data) {
          vm.topic.active = !vm.topic.active;
          vm.translate(['TOPIC_DELETED']).then(translations => {
            ngNotify.set(translations.TOPIC_DELETED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }

  }

})();
