(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('CreateTopicController', CreateTopicController);

  /* @ngInject */
  function CreateTopicController(TopicService, $uibModalInstance, $uibModalStack, ngNotify, survey, SurveyService, $translate) {
    var vm = this;
    vm.survey = survey;
    vm.createTopic = createTopic;
    vm.close = close;
    vm.translate = $translate;

    vm.topic = {
      nameEs: '',
      nameEn: '',
      survey: {
        id: survey.id
      }
    };

    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    function createTopic() {
      TopicService.createTopic(vm.topic)
        .then(function(response) {
          vm.newTopic = {
            id: response.data.id,
            nameES: response.data.nameEs,
            nameEN: response.data.nameEn,
            subtopics: []
          };
          vm.survey.topics.push(vm.newTopic);
          vm.translate(['TOPIC_CREATED']).then(translations => {
            ngNotify.set(translations.TOPIC_CREATED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      close();
    }
  }
})();
