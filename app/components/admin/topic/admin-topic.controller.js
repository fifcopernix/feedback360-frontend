(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('AdminTopicController', AdminTopicController);

  /* @ngInject */
  function AdminTopicController($state, UserService, TopicService, $uibModal, ngNotify, $translate) {
    var vm = this;
    vm.topics = [];
    vm.createTopic = createTopic;
    vm.updateTopic = updateTopic;
    vm.deleteTopic = deleteTopic;
    vm.translate = $translate;

    activate();
    getTopics();

    function activate() {
      UserService.verifyCredentials();
      if (!UserService.isAdmin()) {
        $state.go('home.dashboard');
        vm.translate(['PERMISSION_ERROR']).then(translations => {
          ngNotify.set(translations.PERMISSION_ERROR, 'error');
        });
      }
    }

    function getTopics() {
      TopicService.getTopics()
        .then(function(topicsData) {
          vm.topics = topicsData.data;
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_TOPICS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_TOPICS, 'error');
          });
        });
    }

    function createTopic() {
      $uibModal.open({
        templateUrl: 'app/components/admin/topic/create/create.html',
        controller: 'CreateTopicController as vm',
        resolve: {
          topics: function() {
            return vm.topics;
          }
        }
      });
    }

    function updateTopic(topic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/topic/update/update.html',
        controller: 'UpdateTopicController as vm',
        resolve: {
          topic: function() {
            return topic;
          }
        }
      });
    }

    function deleteTopic(topic) {
      $uibModal.open({
        templateUrl: 'app/components/admin/topic/delete/delete.html',
        controller: 'DeleteTopicController as vm',
        resolve: {
          topic: function() {
            return topic;
          }
        }
      });
    }

  }

})();
