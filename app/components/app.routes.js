(function() {
  'use strict';

  angular
    .module('app')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/login');
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });
  }
})();
