(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('ForgetPasswordController', ForgetPasswordController);

  /* @ngInject */
  function ForgetPasswordController($uibModalInstance,
                                    ngNotify,
                                    UserService,
                                    $translate) {
    var vm = this;
    vm.closeModal = closeModal;
    vm.email;
    vm.recoverPassword = recoverPassword;
    vm.translate = $translate;

    function closeModal() {
      $uibModalInstance.dismiss('cancel');
    }

    function recoverPassword() {
      var language = currentLanguage();
      UserService.recoverPassword(vm.email, language)
        .then(response => {
          vm.translate(['PASSWORD_RESET_EMAIL',]).then(translations => {
            ngNotify.set(translations.PASSWORD_RESET_EMAIL + vm.email, 'success');
          });
          closeModal();
        })
        .catch(error => {
          if (error.status === 404) {
            vm.translate(['USER', 'NOT_EXIST']).then(translations => {
              ngNotify.set(translations.USER + vm.email +  translations.NOT_EXIST, 'error');
            });
          }
          else {
            vm.translate(['ERROR_RESTORING_PASSWORD']).then(translations => {
              ngNotify.set(translations.ERROR_RESTORING_PASSWORD, 'error');
            });
          }
        });
    }

    function currentLanguage() {
      return UserService.getLanguage();
    }

  }

})();
