(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginController', LoginController);

  /* @ngInject */
  function LoginController($http, $state, LoginService, ngNotify, UserService, GraphHelper, $translate, $log, $timeout, $uibModal) {
    var vm = this;
    vm.setLanguage = setLanguage;
    vm.isLanguage = isLanguage;
    vm.displayName;
    vm.emailAddress;
    vm.emailAddressSent;
    vm.requestSuccess;
    vm.requestFinished;
    vm.uibModal = $uibModal;
    vm.forgotPassword = forgotPassword;
    vm.translate = $translate;

    vm.user = {
      username: '',
      password: '',
      office365TokenAuth: '',
      office365Id: '',
      office365Email: '',
      role: {}
    };

    vm.authenticateUser = authenticateUser;
    vm.office365login = office365login;

    setLanguage('es');

    function office365login() {
      GraphHelper.login().then(response => {
        processAuth().then(response => {
          setCurrentUser(vm.user).then(res => {
            UserService.retrieveOffice365Credentials(vm.user).then(res => {
              vm.user= res.data;
              UserService.setCurrentUser(vm.user);
              if (!angular.equals(res, {})) {
                $timeout(function () {
                  $state.go('home.dashboard');
                }, 300);
              } else {
                UserService.clearCurrentUser();
                $state.go('login');
                ngNotify.set('Su cuenta no ha sido asociada con Office365.', 'error');
              }
            }).catch(res => {
              UserService.clearCurrentUser();
              $state.go('login');
              ngNotify.set('Su cuenta no ha sido asociada con Office365.', 'error');
              $log.debug(res);
            });
          });
        });
      }).catch (error => {
        vm.translate(['ERROR']).then(translations => {
          ngNotify.set(translations.ERROR, 'error');
        });
      })
    }

    function setCurrentUser(user) {
      return new Promise((resolve, reject)=>{
        UserService.setCurrentUser(user);
        resolve();
      })
    }
  
    function processAuth() {
      return new Promise((resolve, reject)=> {
        if (sessionStorage.token) {
          $http.defaults.headers.common.Authorization = 'Bearer ' + sessionStorage.token;
          $http.defaults.headers.common.SampleID = 'angular-connect-rest-sample';

          if (sessionStorage.getItem('user') === null) {
            GraphHelper.getPersonalInformation()
              .then(function (response) {
                let user = response.data;
                sessionStorage.setItem('user', angular.toJson(user));
                vm.user.office365TokenAuth = sessionStorage.token;
                vm.user.office365Id = user.id;
                vm.user.office365Email = user.userPrincipalName;
                resolve();
            }).catch(function(error){
              reject();
            });
          } else {
            var userOffice = JSON.parse(sessionStorage.user);
            vm.username = '';
            vm.password = '';
            vm.user.office365TokenAuth = sessionStorage.token;
            vm.user.office365Id = userOffice.id;
            vm.user.office365Email = userOffice.userPrincipalName;
            resolve();
          }
        }
      });
    }

    function authenticateUser() {
      LoginService.login(vm.user.username.toLowerCase(), vm.user.password)
        .then(
          res => {
            UserService.setCurrentUser(vm.user);
            getUserRole(vm.user);
            $timeout(function() {
              $state.go('home.dashboard');
            }, 300);
          }
        )
        .catch(
          res => {
            UserService.clearCurrentUser();
            $state.go('login');
            vm.translate(['LOGIN_ERROR']).then(translations => {
              ngNotify.set(translations.LOGIN_ERROR, 'error');
            });
            $log.debug(res);
          }
        )
    }

    function getUserRole(user) {
      return new Promise((resolve,reject) => {
        return UserService.getUserTypeByLogin(user.username.toLowerCase()).then(res => {
          UserService.setCurrentUser(res.data);
          resolve();
      })
        $log.debug(res);
      }).catch(res => {
        $log.debug(res);
      })
    }

    function setLanguage(lang) {
      UserService.setLanguage(lang);
      $translate.use(lang);
    }

    function isLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function forgotPassword() {
      vm.uibModal.open({
        templateUrl: 'app/components/login/forget-password/forget-password.html',
        controller: 'ForgetPasswordController as vm'
      });
    }

    function isAuthenticated() {
      return localStorage.getItem('user') !== null;
    }
  }
})();

