var APPLICATION_CONFIG = {
    clientID: "ac842b7a-b6f0-4c13-a57e-66c1f32605ac",
    redirectUri: "http://localhost:3000/login",
    interactionMode: "popUp",
    graphEndpoint: "https://graph.microsoft.com/v1.0/me",
    graphScopes: ["user.read mail.send"]
};