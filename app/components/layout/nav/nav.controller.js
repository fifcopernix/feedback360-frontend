(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller('NavController', NavController);

  /* @ngInject */
  function NavController($state, $http, UserService, $translate, EvaluationService, GraphHelper, ngNotify, $uibModal) {
    var vm = this;
    vm.user = {};
    vm.getUser = getUser;
    vm.office365User={};
    vm.isActive = isActive;
    vm.logout = logout;
    vm.isAdmin = isAdmin;
    vm.setLanguage = setLanguage;
    vm.isLanguage = isLanguage;
    vm.resetPassword = resetPassword;
    vm.uibModal = $uibModal;
    vm.sendUserCredentials = sendUserCredentials;

    vm.displayName;
    vm.emailAddress;
    vm.emailAddressSent;
    vm.requestSuccess;
    vm.requestFinished;
    vm.processAuth = processAuth;

    vm.office365login = office365login;
    vm.logout = logout;
    vm.isAuthenticated = isAuthenticated;
    vm.initAuth = initAuth;

    activate();
    getUser();
    function office365login(){
     GraphHelper.login()
      .then((response) => {
        initAuth();
     })
     .catch(error => {

     });
    }
    function processAuth() {
      $http.defaults.headers.common.Authorization = 'Bearer ' + sessionStorage.token;
      $http.defaults.headers.common.SampleID = 'angular-connect-rest-sample';

      if (sessionStorage.getItem('user') === null) {
        GraphHelper.getPersonalInformation()
          .then(function (response) {
          let user = response.data;
          vm.office365User = user;
          sessionStorage.setItem('user', angular.toJson(user));
          vm.displayName = user.displayName;
          vm.emailAddress = user.mail || user.userPrincipalName;
          var userOffice = JSON.parse(sessionStorage.user);
          vm.user.office365TokenAuth = sessionStorage.token;
          vm.user.office365Id = userOffice.id;
          vm.user.office365Email = userOffice.userPrincipalName;
          vm.sendUserCredentials(vm.user);
        });
      } else {
        let user = angular.fromJson(sessionStorage.user);
        var userOffice=JSON.parse(sessionStorage.user);
        vm.user.office365TokenAuth=sessionStorage.token;
        vm.user.office365Id=userOffice.id;
        vm.user.office365Email=userOffice.userPrincipalName;
        vm.sendUserCredentials(vm.user);
      }
    }

    function sendUserCredentials(user){
      if(user){
        UserService.setOffice365Credentials(user)
        .then(function(data){
          ngNotify.set('User has been updated successfully', 'success');
        })
        .catch(function(error){
          ngNotify.set('An error has been occurred, please try again', 'error');
        });
      }
    }

    function isAuthenticated() {
      return sessionStorage.getItem('user') !== null;
    }

    function office365logout() {
      GraphHelper.logout();
    }

    function initAuth() {
      if (sessionStorage.token) {
        processAuth();
      }
    }

    function isLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function setLanguage(lang) {
      UserService.setLanguage(lang);
      $translate.use(lang);
      $state.reload();
    }

    function activate() {
      UserService.verifyCredentials();
      let language = UserService.getLanguage();
      $translate.use(language);
    }

    function isActive(viewLocation) {
      return viewLocation === $state.current.name;
    }

    function getUser() {
      vm.user = UserService.getCurrentUser();
    }

    function logout() {
      UserService.clearCurrentUser();
      EvaluationService.clearSessionEvaluation();
      sessionStorage.clear();
      localStorage.clear();
      vm.user={};
      $state.go('login');
      location.reload();
    }

    function isAdmin() {
      return UserService.isAdmin();
    }

    function resetPassword() {
      vm.uibModal.open({
        templateUrl: 'app/components/reset-password/reset-password.html',
        controller: 'ResetPasswordController as vm'
      });
    }

  }
})();
