(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller('AdminMenuController', AdminMenuController);

  /* @ngInject */
  function AdminMenuController(UserService, $state) {
    var vm = this;
    vm.visible = UserService.isAdmin();
    vm.isActive = isActive;
    activate();

    function activate() {
      UserService.verifyCredentials();
    }

    function isActive(viewLocation) {
      return viewLocation === $state.current.name;
    }
  }
})();
