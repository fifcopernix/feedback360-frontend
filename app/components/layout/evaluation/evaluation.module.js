(function() {
  'use strict';

  angular
    .module('app.layout')
    .component('evaluation', {
      templateUrl: 'app/components/layout/evaluation/evaluation-tpl.html',
      controller: 'EvaluationController as vm'
    });
})();
