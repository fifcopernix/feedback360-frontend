(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller(
      'EvaluationController',
      EvaluationController
      );

  /* @ngInject */
  function EvaluationController(EvaluationService, ngNotify, $translate) {
    var vm = this;
    vm.evaluations = [];
    vm.datapoints = [];
    vm.datacolumns = [{'id': 'top-1', 'type': 'bar', 'name': 'Evaluations'}];
    vm.datax = {'id': 'x'};
    vm.translate = $translate;

    // getEvaluations();

    function getEvaluations() {
      var tmpList = [];
      EvaluationService.getEvaluations()
        .then(function(evaluationsData) {
          vm.evaluations = evaluationsData.data;
          _.forEach(evaluationsData.data, function(value, key) {
            vm.datapoints.push({'x': value.name + ' - ' + value.onTrack.name, 'top-1': value.percentage});
          });
        })
        .catch(function(error) {ERROR_LOADING_EVALUATIONS
          vm.translate(['ERROR_SENDING_ANSWERS']).then(translations => {
            ngNotify.set(translations.ERROR_SENDING_ANSWERS, 'error');
          });
        });
    }
  }
})();
