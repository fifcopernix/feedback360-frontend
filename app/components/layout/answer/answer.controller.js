(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller(
      'AnswerController',
      AnswerController
      );

  /* @ngInject */
  function AnswerController(AnswerService, ngNotify, RESOURCE, $scope, UserService, $translate) {
    var vm = this;
    vm.answers = [];
    vm.translate = $translate;

    activate();

    function activate() {
      UserService.verifyCredentials();
    }

    function getColor(value) {
      return RESOURCE.PALETTE[Math.round(value / 10)];
    }

    function getMetric(path) {
      vm.answers = [];
      AnswerService.getMetric(path)
        .then(function(data) {
          _.forEach(data.data, function(value, key) {
            var metric = {};
            metric.id = value.id;
            metric.name = value.name;
            metric.value = (value.value * 100) / 5;
            metric.color = getColor(metric.value);
            vm.answers.push(metric);
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR_LOADING_INDICATORS']).then(translations => {
            ngNotify.set(translations.ERROR_LOADING_INDICATORS, 'success');
          });
        });
    }

  }
})();
