(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardEvaluationController', DashboardEvaluationController);

  /* @ngInject */
  function DashboardEvaluationController(EvaluationService, UserService, $uibModal, $stateParams, $filter, $state, $log) {
    var vm = this;
    vm.translate = $filter('translate');
    vm.evaluation = $stateParams.evaluation || EvaluationService.getSavedEvaluation();
    vm.currentLanguage = currentLanguage;
    vm.addEvaluators = addEvaluators;
    vm.evaluationCompleted = evaluationCompleted;
    vm.evaluationResults = evaluationResults;
    vm.finishEvaluationModal = finishEvaluationModal;
    vm.currentEvaluationStep = 0;
    vm.currentEvaluationStatus = '';

    activate();

    function activate() {
      getEvaluationStatus();
    }

    function currentLanguage() {
      return UserService.getLanguage();
    }

    function getEvaluationStatus() {
      var status = vm.evaluation.status;

      if (status == 'STARTED') {
        vm.currentEvaluationStep = 1;
        vm.currentEvaluationStatus = vm.translate('EV_STEP_1');
      } else if (status == 'INVITATIONS_SENT') {
        vm.currentEvaluationStep = 2;
        vm.currentEvaluationStatus = vm.translate('EV_STEP_2');
      } else if (status == 'IN_PROGRESS') {
        vm.currentEvaluationStep = 3;
        vm.currentEvaluationStatus = vm.translate('EV_STEP_3');
      } else if (status == 'COMPLETED') {
        vm.currentEvaluationStep = 4;
        vm.currentEvaluationStatus = vm.translate('EV_STEP_4');
      } else {
        vm.currentEvaluationStep = 0;
        vm.currentEvaluationStatus = '';
      }
    }

    function addEvaluators(evaluation) {
      $uibModal.open({
        templateUrl: 'app/components/dashboard/evaluators/create.html',
        controller: 'DashboardEvaluatorCreateController as vm',
        resolve: {
          evaluation: function() {
            return evaluation;
          }
        }
      });
    }

    function evaluationCompleted(ev) {
      return ev.status ==='COMPLETED';
    }

    function evaluationResults(evaluation) {
      (vm.currentLanguage() === 'es') ? 
        $state.go('home.results', { evaluation: evaluation }):
        $state.go('home.resultsEn', { evaluation: evaluation });
    }

    function finishEvaluationModal(evaluation) {
      $uibModal.open({
        templateUrl: 'app/components/dashboard/evaluations/confirm-dialog.html',
        controller: 'DashboardConfirmDialogController as vm',
        resolve: {
          evaluation: function() {
            return evaluation;
          }
        }
      });
    }
  }
})();
