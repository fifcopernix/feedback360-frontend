(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('EvaluationResultsController', EvaluationResultsController);

  /* @ngInject */
  function EvaluationResultsController(EvaluationService, UserService, $stateParams, ngNotify, $translate) {
    var vm = this;
    vm.evaluation = $stateParams.evaluation || EvaluationService.getSavedEvaluation();
    vm.currentLanguage = currentLanguage;
    vm.translate = $translate;

    vm.generalChart = {
      palette: 'Default',
      title: 'Resultados Generales',
      commonSeriesSettings: {
        argumentField: 'name',
        type: 'stackedbar'
      },
      commonAxisSettings: {
        label: {
          overlappingBehavior: 'none'
        }
      },
      margin: {
        bottom: 50,
        left: 100
      },
      argumentAxis: {
        discreteAxisDivisionMode: 'crossLabels',
        firstPointOnStartAngle: true
      },
      valueAxis: {
        valueMarginsEnabled: false
      },
      series: [
        {valueField: 'selfAssessmentGrade', name: 'Autoevaluación'},
        {valueField: 'evaluatorsAverageGrade', name: 'Resto de evaluadores'}
      ],
      bindingOptions: {
        dataSource: 'vm.genChartValues'
      }
    };

    vm.genChartValues = [];

    vm.generalResults = {};

    vm.autoEvalChart = {
      palette: 'Default',
      title: 'Autoevaluación',
      commonSeriesSettings: {
        argumentField: 'name',
        type: 'stackedbar'
      },
      commonAxisSettings: {
        label: {
          overlappingBehavior: 'none'
        }
      },
      margin: {
        bottom: 50,
        left: 100
      },
      argumentAxis: {
        discreteAxisDivisionMode: 'crossLabels',
        firstPointOnStartAngle: true
      },
      valueAxis: {
        valueMarginsEnabled: false
      },
      series: [
        {valueField: 'grade', name: 'Autoevaluación'}
      ],
      bindingOptions: {
        dataSource: 'vm.autoEvalChartValues'
      }
    };
    vm.autoEvalChartValues = [];

    vm.leaderChart = {
      palette: 'Default',
      title: 'Jefaturas',
      commonSeriesSettings: {
        argumentField: 'name',
        type: 'stackedbar'
      },
      commonAxisSettings: {
        label: {
          overlappingBehavior: 'none'
        }
      },
      margin: {
        bottom: 50,
        left: 100
      },
      argumentAxis: {
        discreteAxisDivisionMode: 'crossLabels',
        firstPointOnStartAngle: true
      },
      valueAxis: {
        valueMarginsEnabled: false
      },
      series: [
        {valueField: 'grade', name: 'Jefaturas'}
      ],
      bindingOptions: {
        dataSource: 'vm.leaderChartValues'
      }
    };
    vm.leaderChartValues = [];

    vm.peerChart = {
      palette: 'Default',
      title: 'Pares',
      commonSeriesSettings: {
        argumentField: 'name',
        type: 'stackedbar'
      },
      commonAxisSettings: {
        label: {
          overlappingBehavior: 'none'
        }
      },
      margin: {
        bottom: 50,
        left: 100
      },
      argumentAxis: {
        discreteAxisDivisionMode: 'crossLabels',
        firstPointOnStartAngle: true
      },
      valueAxis: {
        valueMarginsEnabled: false
      },
      series: [
        {valueField: 'grade', name: 'Pares'}
      ],
      bindingOptions: {
        dataSource: 'vm.peerChartValues'
      }
    };
    vm.peerChartValues = [];

    vm.collaboratorChart = {
      palette: 'Default',
      title: 'Colaboradores',
      commonSeriesSettings: {
        argumentField: 'name',
        type: 'stackedbar'
      },
      commonAxisSettings: {
        label: {
          overlappingBehavior: 'none'
        }
      },
      margin: {
        bottom: 50,
        left: 100
      },
      argumentAxis: {
        discreteAxisDivisionMode: 'crossLabels',
        firstPointOnStartAngle: true
      },
      valueAxis: {
        valueMarginsEnabled: false
      },
      series: [
        {valueField: 'grade', name: 'Colaboradores'}
      ],
      bindingOptions: {
        dataSource: 'vm.collaboratorChartValues'
      }
    };
    vm.collaboratorChartValues = [];

    vm.otherChart = {
      palette: 'Default',
      title: 'Reportes directos',
      commonSeriesSettings: {
        argumentField: 'name',
        type: 'stackedbar'
      },
      commonAxisSettings: {
        label: {
          overlappingBehavior: 'none'
        }
      },
      margin: {
        bottom: 50,
        left: 100
      },
      argumentAxis: {
        discreteAxisDivisionMode: 'crossLabels',
        firstPointOnStartAngle: true
      },
      valueAxis: {
        valueMarginsEnabled: false
      },
      series: [
        {valueField: 'grade', name: 'Reportes directos'}
      ],
      bindingOptions: {
        dataSource: 'vm.otherChartValues'
      }
    };
    vm.otherChartValues = [];

    vm.bestQuestions = [];
    vm.worstQuestions = [];
    vm.openQuestions = [];
    translateCharts();
    function translateCharts(){
      if(sessionStorage.getItem('language')==='en'){
        vm.generalChart.title='General Results';
        vm.generalChart.series[0].name='Self-Evaluation';
        vm.generalChart.series[1].name='Rest of evaluators';
        vm.autoEvalChart.title='Self-Evaluation';
        vm.autoEvalChart.series[0].name='Self-Evaluation';
        vm.leaderChart.title='Leaders';
        vm.leaderChart.series[0].name='Leaders';
        vm.peerChart.title='Peers';
        vm.peerChart.series[0].name='Peers';
        vm.collaboratorChart.title='Contributors';
        vm.collaboratorChart.series[0].name='Contributors';
        vm.otherChart.title='Direct Reports';
        vm.otherChart.series[0].name='Direct Reports';
      }
    }

    activate();

    function activate() {
      getEvaluatioResults(vm.evaluation);
    }

    function getEvaluatioResults(evaluation) {
      EvaluationService.getEvaluationResults(evaluation)
        .then(function (response) {
          vm.generalResults = response.data.overallResults;
          vm.genChartValues = getSubtopicsArray(response.data.overallResults);
          vm.autoEvalChartValues = getSubtopicsArray(response.data.selfAssessmentResults);
          vm.leaderChartValues = getSubtopicsArray(response.data.leaderEvaluatorsResults);
          vm.peerChartValues = getSubtopicsArray(response.data.peerEvaluatorsResults);
          vm.collaboratorChartValues = getSubtopicsArray(response.data.collaboratorEvaluatorsResults);

          vm.bestQuestions = response.data.bestQuestions;
          vm.worstQuestions = response.data.worstQuestions;

          vm.openQuestions = response.data.openQuestions;
        })
        .catch(function (error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function getSubtopicsArray(topics) {
      let array = [];

      topics.forEach(function (topic) {
        array = array.concat(topic.subtopics);
      });

      return array;
    }
  }
})();
