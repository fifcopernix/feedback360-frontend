(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardConfirmDialogController', DashboardConfirmDialogController);

  /* @ngInject */
  function DashboardConfirmDialogController(EvaluationService, UserService, ngNotify, evaluation, $uibModalInstance, $translate) {
    var vm = this;
    vm.evaluation = evaluation;
    vm.currentLanguage = currentLanguage;
    vm.finishEvaluation = finishEvaluation;
    vm.closeModal = closeModal;
    vm.translate = $translate;

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function finishEvaluation(ev) {
      ev.status = 'COMPLETED';
      EvaluationService.updateEvaluation(ev)
        .then(function(response) {
          vm.evaluation = response.data;
          EvaluationService.clearSessionEvaluation();
          EvaluationService.saveEvaluationToSession(vm.evaluation);
          vm.translate(['EVALUATION_COMPLETED']).then(translations => {
            ngNotify.set(translations.EVALUATION_COMPLETED, 'success');
          });
        })
        .catch(function(error) {
          vm.translate(['ERROR']).then(translations => {
            ngNotify.set(translations.ERROR, 'error');
          });
        });
      closeModal();
    }

    function closeModal() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
