(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardController', DashboardController);

  /* @ngInject */
  function DashboardController(EvaluationService, UserService, $state, $timeout, $log, $window, $translate, ngNotify) {
    var vm = this;

    vm.user = UserService.getCurrentUser();
    vm.reload = reload;
    vm.participatingEvaluations = [];
    vm.userEvaluations = [];
    vm.completedEvaluation = completedEvaluation;
    vm.currentLanguage = currentLanguage;
    vm.detailEvaluation = detailEvaluation;
    vm.getFormLink = getFormLink;
    vm.evaluatorCompletedEvaluation = evaluatorCompletedEvaluation;
    vm.translate = $translate;

    activate();

    function activate() {
      UserService.verifyCredentials();

      if(typeof vm.user === null) {
        vm.user = UserService.getCurrentUser();
      }

      $timeout(function() {
        evaluationsWhereUserParticipates();
        getEvaluationsByUser();
      }, 500);
    }

    function evaluationsWhereUserParticipates() {
      EvaluationService.getEvaluationsByEvaluator(vm.user)
        .then(function (response) {
          vm.participatingEvaluations = response.data;
        }).catch(function (response) {
        vm.participatingEvaluations = [];
      })
    }

    function getEvaluationsByUser() {
      EvaluationService.getEvaluationsByUser(vm.user)
        .then(function (response) {
          vm.userEvaluations = response.data;
        }).catch(function (error) {
        vm.userEvaluations = [];
      })
    }

    function reload() {
      $state.reload();
    }

    function completedEvaluation(ev) {
      if (ev.status == 'COMPLETED') {
        return 'completed-evaluation';
      } else {
        return 'pending-evaluation';
      }
    }

    function getFormLink(ev) {
      let evaluators = ev.evaluators;
      let currentUser = UserService.getCurrentUser();
      let link = '';
      for (let evaluator of evaluators) {
        if (null !== evaluator.user) {
          EvaluationService.checkEvaluationCompleted(evaluator.id).then((response) => {
          let status = response.data;
          if (evaluator.user.id === currentUser.id && status!==true) {
            link = `/#/evaluations?key=${evaluator.uniqueLink}`;
            $window.open(link, '_blank');
            return;
          }
          }).catch(error => {
            vm.translate(['EVALUATION_OUT_OF_DATE']).then(translations => {
              ngNotify.set(translations.EVALUATION_OUT_OF_DATE, 'error');
            });
          });
          if(evaluator.user.id === currentUser.id || status===true) {
            vm.translate(['EVALUATION_IN_PROGRESS']).then(translations => {
              ngNotify.set(translations.EVALUATION_IN_PROGRESS, 'error');
            });
          }
        }
      }
    }

    function evaluatorCompletedEvaluation(ev) {
      let evaluators = ev.evaluators;
      let currentUser = UserService.getCurrentUser();
      let result = '';

      for (let evaluator of evaluators) {
        if (null !== evaluator.user) {
          if (evaluator.user.id === currentUser.id) {
            result = evaluator.status === 'COMPLETED';
            break;
          }
        }
      }
      return result;
    }

    function detailEvaluation(evaluation) {
      EvaluationService.saveEvaluationToSession(evaluation);
      $state.go('home.evaluation', { evaluation: evaluation });
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function setLanguage(lang) {
      UserService.setLanguage(lang);
      $translate.use(lang);
      $state.reload();
    }
  }
})();
