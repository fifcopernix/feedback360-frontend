(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardEvaluatorCreateController', DashboardEvaluatorCreateController);

  /* @ngInject */
  function DashboardEvaluatorCreateController(EvaluationService, UserService, ngNotify, evaluation, $uibModalInstance, $log, $filter, $translate) {
    var vm = this;
    vm.translate = $translate;
    vm.evaluation = evaluation;
    vm.currentLanguage = currentLanguage;
    vm.createEvaluators = createEvaluators;
    vm.closeModal = closeModal;
    vm.addEvaluator = addEvaluator;
    vm.removeEvaluator = removeEvaluator;
    vm.checkEvaluators = checkEvaluators;
    vm.roles = [
      {
        nameEs: 'Jefe',
        nameEn: 'Leader'
      },
      {
        nameEs: 'Colaborador',
        nameEn: 'Collaborator'
      },
      {
        nameEs: 'Par',
        nameEn: 'Peer'
      },
      {
        nameEs: 'Otro',
        nameEn: 'Other'
      }
    ];

    vm.request = {
      evaluation: {
        id: evaluation.id
      },
      evaluators: [{
        name: '',
        role: '',
        email: ''
      }],
      language: null
    };

    activate();

    function activate() {
    }

    function checkEvaluators() {
      let leader = 0,
        peer = 0,
        collaborator = 0;

      vm.request.evaluators.forEach(evaluator => {
        switch (evaluator['role']) {
          case 'Jefe':
            leader++;
            break;

          case 'Par':
            peer++;
            break;

          case 'Colaborador':
            collaborator++;
            break;

          default:

        }
      })
      if ((leader > 0 && peer > 1 && collaborator > 1) || evaluation.evaluators.length > 1) {
        vm.createEvaluators();
      } else {
        vm.translate(['EVALUATOR_LIMITS']).then(translations => {
          ngNotify.set(translations.EVALUATOR_LIMITS, 'error');
        });
      }
    }

    function createEvaluators() {
      vm.request.language = UserService.getLanguage();
      if (!compareEvaluator()) {
        EvaluationService.inviteEvaluators(vm.request)
          .then(function (response) {
            evaluation.evaluators = evaluation.evaluators.concat(response.data)
            vm.translate(['EVALUATORS_INVITED']).then(translations => {
              ngNotify.set(translations.EVALUATORS_INVITED, 'success');
            });
          })
          .catch(function (error) {
            vm.translate(['ERROR']).then(translations => {
              ngNotify.set(translations.ERROR, 'error');
            });
          })
        closeModal();
      } else {
        vm.translate(['EVALUATOR_REPEATED']).then(translations => {
          ngNotify.set(translations.EVALUATOR_REPEATED, 'error');
        });
      }

    }

    function addEvaluator() {
      vm.request.evaluators.push({ name: '', role: '', email: '' });
    }

    function removeEvaluator() {
      var lastEvaluator = vm.request.evaluators.length - 1;
      vm.request.evaluators.splice(lastEvaluator);
    }

    function compareEvaluator() {
      for (var i = 0; i < vm.request.evaluators.length; i++) {
        for (var x = i + 1; x < vm.request.evaluators.length; x++) {
          if (vm.request.evaluators[i].email === vm.request.evaluators[x].email) {
            return true
          }
        }
      }
      return false;
    }

    function currentLanguage(lang) {
      return UserService.getLanguage() == lang;
    }

    function closeModal() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
