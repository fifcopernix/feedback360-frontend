(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home.dashboard', {
        url: '/dashboard',
        templateUrl: 'app/components/dashboard/dashboard.html',
        controller: 'DashboardController as vm'
      })
      .state('home.evaluation', {
        url: '/evaluation',
        templateUrl: 'app/components/dashboard/evaluations/dashboard-evaluation.html',
        controller: 'DashboardEvaluationController as vm',
        params: {
          evaluation: null
        }
      })
      .state('home.results', {
        url: '/evaluation/results',
        templateUrl: 'app/components/dashboard/evaluations/results/evaluation-results.html',
        controller: 'EvaluationResultsController as vm',
        params: {
          evaluation: null
        }
      })
      .state('home.resultsEn', {
        url: '/evaluation/results-en',
        templateUrl: 'app/components/dashboard/evaluations/results/evaluation-results_en.html',
        controller: 'EvaluationResultsController as vm',
        params: {
          evaluation: null
        }
      });
  }
})();
